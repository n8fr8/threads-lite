/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.tls;


import net.luminis.tls.handshake.HandshakeMessage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


// https://tools.ietf.org/html/rfc8446#section-4.4.1
// "Many of the cryptographic computations in TLS make use of a transcript hash. This value is computed by hashing the
//  concatenation of each included handshake message, including the handshake message header carrying the handshake
//  message type and length fields, but not including record layer headers."
public class TranscriptHash {

    // https://tools.ietf.org/html/rfc8446#section-4.4.1
    // "For concreteness, the transcript hash is always taken from the
    //   following sequence of handshake messages, starting at the first
    //   ClientHello and including only those messages that were sent:
    //   ClientHello, HelloRetryRequest, ClientHello, ServerHello,
    //   EncryptedExtensions, server CertificateRequest, server Certificate,
    //   server CertificateVerify, server Finished, EndOfEarlyData, client
    //   Certificate, client CertificateVerify, client Finished."
    private static final ExtendedHandshakeType[] hashedMessages = {
            ExtendedHandshakeType.client_hello,
            ExtendedHandshakeType.server_hello,
            ExtendedHandshakeType.encrypted_extensions,
            ExtendedHandshakeType.certificate_request,
            ExtendedHandshakeType.server_certificate,
            ExtendedHandshakeType.server_certificate_verify,
            ExtendedHandshakeType.server_finished,
            ExtendedHandshakeType.client_certificate,
            ExtendedHandshakeType.client_certificate_verify,
            ExtendedHandshakeType.client_finished
    };
    private final MessageDigest hashFunction;
    private final Map<Integer, byte[]> msgData = new ConcurrentHashMap<>();
    private final Map<Integer, byte[]> hashes = new ConcurrentHashMap<>();

    public TranscriptHash(int hashLength) {
        // https://tools.ietf.org/html/rfc8446#section-7.1
        // "The Hash function used by Transcript-Hash and HKDF is the cipher suite hash algorithm."
        String hashAlgorithm = "SHA-" + (hashLength * 8);
        try {
            hashFunction = MessageDigest.getInstance(hashAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing " + hashAlgorithm + " support");
        }
    }

    /**
     * Return the transcript hash for the messages in the handshake up to and including the indicated message type.
     */
    public byte[] getHash(HandshakeType msgType) {
        return getHash(convert(msgType));
    }

    /**
     * Return the transcript hash for the messages in the handshake up to and including the indicated client message type.
     * For example, when the <code>msgType</code> parameter has value <code>certificate</code>, the transcript hash for
     * the concatenation of handshake messages up to (and including) the client certificate message is returned.
     */
    public byte[] getClientHash(HandshakeType msgType) {
        return getHash(convert(msgType, true));
    }

    /**
     * Return the transcript hash for the messages in the handshake up to and including the indicated server message type.
     * For example, when the <code>msgType</code> parameter has value <code>certificate</code>, the transcript hash for
     * the concatenation of handshake messages up to (and including) the server certificate message is returned.
     */
    public byte[] getServerHash(HandshakeType msgType) {
        return getHash(convert(msgType, false));
    }

    /**
     * Record a handshake message for computing the transcript hash. The type of the message determines its position
     * in the transcript hash computation.
     */
    public void record(HandshakeMessage msg) {
        List<HandshakeType> ambigousTypes = List.of(HandshakeType.certificate,
                HandshakeType.certificate_verify, HandshakeType.finished);
        if (ambigousTypes.contains(msg.getType())) {
            throw new IllegalArgumentException();
        }
        msgData.put(convert(msg.getType()), msg.getBytes());
    }

    /**
     * Record a client handshake message for computing the transcript hash. This method is needed because the
     * <code>TlsConstants.HandshakeType</code> type does not differentiate between client and server variants, whilst
     * these variants have a different position in the transcript hash computation.
     * Note that the term "client" here refers to the message type, not whether it is sent or received by a client.
     * For example, a client certificate message is sent by the client and received by the server; both need to use
     * this method to record the message.
     */
    public void recordClient(HandshakeMessage msg) {
        msgData.put(convert(msg.getType(), true), msg.getBytes());
    }

    /**
     * Record a server handshake message for computing the transcript hash. This method is needed because the
     * <code>TlsConstants.HandshakeType</code> type does not differentiate between client and server variants, whilst
     * these variants have a different position in the transcript hash computation.
     * Note that the term "server" here refers to the message type, not whether it is sent or received by a server.
     * For example, a server certificate message is sent by the server and received by the client; both need to use
     * this method to record the message.
     */
    public void recordServer(HandshakeMessage msg) {
        msgData.put(convert(msg.getType(), false), msg.getBytes());
    }

    private byte[] getHash(int ordinal) {
        if (!hashes.containsKey(ordinal)) {
            computeHash(ExtendedHandshakeType.get(ordinal));
        }
        return hashes.get(ordinal);
    }

    private void computeHash(ExtendedHandshakeType requestedType) {
        for (ExtendedHandshakeType type : hashedMessages) {
            byte[] data = msgData.get(type.ordinal());
            if (data != null) {
                hashFunction.update(data);
            }
            if (type == requestedType) {
                break;
            }
        }
        hashes.put(requestedType.ordinal(), hashFunction.digest());
    }

    private int convert(HandshakeType type) {
        List<HandshakeType> ambigousTypes = List.of(HandshakeType.certificate,
                HandshakeType.certificate_verify, HandshakeType.finished);
        if (ambigousTypes.contains(type)) {
            throw new IllegalArgumentException("cannot convert ambiguous type " + type);
        }
        return type.ordinal();
    }

    private int convert(HandshakeType type, boolean client) {
        if (type == HandshakeType.finished) {
            return client ? ExtendedHandshakeType.client_finished.ordinal() : ExtendedHandshakeType.server_finished.ordinal();
        } else if (type == HandshakeType.certificate) {
            return client ? ExtendedHandshakeType.client_certificate.ordinal() : ExtendedHandshakeType.server_certificate.ordinal();
        } else if (type == HandshakeType.certificate_verify) {
            return client ? ExtendedHandshakeType.client_certificate_verify.ordinal() : ExtendedHandshakeType.server_certificate_verify.ordinal();
        }
        return type.ordinal();
    }
}
