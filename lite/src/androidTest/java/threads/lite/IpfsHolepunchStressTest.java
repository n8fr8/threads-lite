package threads.lite;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;

public class IpfsHolepunchStressTest {
    private static final String TAG = IpfsHolepunchStressTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_holepunch_stress() throws Throwable {

        IPFS ipfs = TestEnv.getTestInstance(context);

        AtomicBoolean connected = new AtomicBoolean(false);

        try (Session session = ipfs.createSession(false)) {

            String link = ipfs.resolveDnsLink("blog.ipfs.io");

            TestCase.assertNotNull(link);
            assertFalse(link.isEmpty());
            Cid cid = Cid.decode(link.replace(IPFS.IPFS_PATH, ""));


            TimeoutCancellable timeout = new TimeoutCancellable(connected::get, 120);
            Set<PeerId> peers = ConcurrentHashMap.newKeySet();
            ipfs.findProviders(session, multiaddr -> {

                // do not do more
                if (timeout.isCancelled()) {
                    return;
                }

                if (multiaddr.isCircuitAddress()) {
                    try {
                        LogUtils.error(TAG, "Try connect " + multiaddr);
                        Connection connection = ipfs.dial(session,
                                multiaddr, ipfs.getConnectionParameters());
                        assertNotNull(connection);

                        LogUtils.error(TAG, ipfs.getPeerInfo(connection).get(IPFS.CONNECT_TIMEOUT,
                                TimeUnit.SECONDS).toString());

                        connected.set(true);
                    } catch (Throwable throwable) {
                        LogUtils.debug(TAG, "Try error " + throwable.getClass().getSimpleName());
                    }
                }
            }, peers::add, cid, timeout);

            for (PeerId peer : peers) {
                LogUtils.info(TAG, "Provider Peer " + peer);
            }
        }
        assertTrue(connected.get());

    }


}
