package threads.lite;


import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.core.PeerInfo;

@RunWith(AndroidJUnit4.class)
public class IpfsIdentifyServiceTest {
    private static final String TAG = IpfsIdentifyServiceTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void identify_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);


        PeerInfo info = ipfs.getIdentity();
        assertNotNull(info);

        assertNotNull(info.getAgent());
        assertNotNull(info.getPeerId());
        assertNotNull(info.getVersion());

        Multiaddrs list = info.getMultiaddrs();
        assertNotNull(list);
        for (Multiaddr addr : list) {
            LogUtils.info(TAG, addr.toString());
        }

    }
}
