package threads.lite;

import static org.junit.Assert.assertNotNull;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.AutonatResult;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.host.LiteServer;

class TestEnv {

    private static final String TAG = TestEnv.class.getSimpleName();
    private static final AtomicBoolean started = new AtomicBoolean(false);

    @NonNull
    private static final AtomicReference<LiteServer> liteServer = new AtomicReference<>();


    @Nullable
    public static Server getServer() {
        return liteServer.get();
    }

    public static void setSequence(Context context, int sequence) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                TestEnv.class.getSimpleName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("SEQ", sequence);
        editor.apply();
    }

    public static int getSequence(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                TestEnv.class.getSimpleName(), Context.MODE_PRIVATE);
        return sharedPref.getInt("SEQ", 0);
    }

    @NonNull
    public static File createCacheFile(Context context) throws IOException {
        return File.createTempFile("temp", ".cid", context.getCacheDir());
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }

    public static IPFS getTestInstance(@NonNull Context context) throws Exception {


        IPFS ipfs = IPFS.getInstance(context);
        ipfs.getBlockStore().clear(); // clears the default blockStore


        if (!started.getAndSet(true)) {

            Server server = ipfs.startServer(5001,
                    connection -> LogUtils.error(TAG, "Incoming connection : "
                            + connection.getRemoteAddress()),
                    connection -> LogUtils.error(TAG, "Closing connection : "
                            + connection.getRemoteAddress()),
                    peerId -> {
                        LogUtils.error(TAG, "Peer Gated : " + peerId.toString());
                        return false;
                    });
            if (Network.isNetworkConnected(context)) {
                AutonatResult result = ipfs.autonat(server);
                LogUtils.error(TAG, "Autonat : " + result);

                LogUtils.error(TAG, "Nat Type " + result.getNatType());
                LogUtils.error(TAG, "Success " + result.success());
                LogUtils.error(TAG, "Address " + result.dialableAddress());

                Set<Reservation> reservations = ipfs.reservations(
                        server, 10, 120);

                for (Reservation reservation : reservations) {
                    LogUtils.error(TAG, reservation.toString());
                }

                for (Multiaddr ma : ipfs.getIdentity().getMultiaddrs()) {
                    LogUtils.error(TAG, "Listen Address " + ma.toString());
                }

                LogUtils.error(TAG, "Next Reservation Cycle " +
                        ipfs.nextReservationCycle() + " [min]");


                assertNotNull(server.getSocket());


                Set<Multiaddr> set = ipfs.dialableAddresses(server);
                for (Multiaddr addr : set) {
                    LogUtils.error(TAG, "Dialable Address " + addr.toString());
                }
            }
        }

        System.gc();
        return ipfs;
    }


}
