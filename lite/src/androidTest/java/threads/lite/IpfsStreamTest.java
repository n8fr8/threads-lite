package threads.lite;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.core.Link;
import threads.lite.core.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsStreamTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_string() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String text = "Hello Moin und Zehn Elf";
            Cid hash = ipfs.storeText(session, text);
            assertNotNull(hash);
            List<Link> links = ipfs.links(session, hash, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);


            byte[] result = ipfs.getData(session, hash, () -> false);
            assertNotNull(result);
            assertEquals(text, new String(result));


            Cid hash2 = ipfs.storeText(session, "TEST test");
            assertNotNull(hash2);
            links = ipfs.links(session, hash2, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);
        }

    }
}
