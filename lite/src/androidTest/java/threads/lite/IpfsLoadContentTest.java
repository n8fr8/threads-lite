package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;

import crypto.pb.Crypto;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.cid.Prefix;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsLoadContentTest {
    private static final String TAG = IpfsLoadContentTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void find_peer_freedomReport() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            //Freedom Report ipns://k51qzi5uqu5dlnwjrnyyd6sl2i729d8qjv1bchfqpmgfeu8jn1w1p4q9x9uqit

            String key = "k51qzi5uqu5dlnwjrnyyd6sl2i729d8qjv1bchfqpmgfeu8jn1w1p4q9x9uqit";

            IpnsEntity res = ipfs.resolveName(session, PeerId.decode(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.error(TAG, res.toString());

            assertEquals(res.getType(), Crypto.KeyType.Ed25519.name());


            Cid cid = ipfs.decodeIpnsData(res);

            Prefix prefix = cid.getPrefix();
            LogUtils.error(TAG, prefix.toString());
            assertTrue(prefix.isSha2556());
            LogUtils.error(TAG, prefix.getType().name());

            Cid root = ipfs.resolveCid(session, cid, Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(root);
            assertEquals(cid, root);

            Cid node = ipfs.resolveCid(session, cid, List.of(IPFS.INDEX_HTML), new TimeoutCancellable(60));
            assertNotNull(node);

            String text = ipfs.getText(session, node, new TimeoutCancellable(30));

            assertNotNull(text);
            TestCase.assertFalse(text.isEmpty());
        }
    }

}
