package threads.lite;

import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.cid.Cid;
import threads.lite.core.Session;

public class IpfsTimeTest {
    private static final String TAG = IpfsTimeTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_adding_performance() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);

        long start = System.currentTimeMillis();
        // create dummy session
        int maxNumberBytes = 100 * 1000 * 1000; // 100 MB
        try (Session session = ipfs.createSession(new DummyBlockStore(), false)) {
            AtomicInteger counter = new AtomicInteger(0);
            Cid cid = ipfs.storeInputStream(session, new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });
            assertNotNull(cid);
        }
        long end = System.currentTimeMillis();
        LogUtils.error(TAG, "Time for hashing " + (end - start) / 1000 + "[s]");


        start = System.currentTimeMillis();
        try (Session session = ipfs.createSession()) {
            AtomicInteger counter = new AtomicInteger(0);
            Cid cid = ipfs.storeInputStream(session, new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });
            assertNotNull(cid);
        }
        end = System.currentTimeMillis();
        LogUtils.error(TAG, "Time for hashing and storing " + (end - start) / 1000 + "[s]");
    }

}
