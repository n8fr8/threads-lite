package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.core.Reservation;
import threads.lite.core.Session;


@RunWith(AndroidJUnit4.class)
public class IpfsBootstrapTest {
    private static final String TAG = IpfsBootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_bootstrap_hops() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);


        try (Session ignored = ipfs.createSession()) {

            assertNotNull(ignored.getPeerStore());

            int timeInMinutes = 1; // make higher for long run

            assertTrue(ipfs.hasReservations());

            int numReservation = ipfs.numReservations();

            for (Multiaddr ma : ipfs.getIdentity().getMultiaddrs()) {
                LogUtils.debug(TAG, ma.toString());
            }

            // test 1 minutes
            for (int i = 0; i < timeInMinutes; i++) {
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));

                Set<Reservation> reservations = ipfs.reservations();
                for (Reservation reservation : reservations) {
                    LogUtils.debug(TAG, "Expire in minutes " + reservation.expireInMinutes()
                            + " " + reservation);
                    Connection conn = reservation.getConnection();
                    assertNotNull(conn);
                    assertTrue(conn.isConnected());
                    assertNotNull(conn.getRemoteAddress());
                }

                assertEquals(numReservation, ipfs.numReservations());
            }
        }
    }

}
