package threads.lite;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.lite.utils.TimeoutProgress;


@RunWith(AndroidJUnit4.class)
public class IpfsHmatShardTest {

    private static final String TAG = IpfsHmatShardTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void hamtShard_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid cid = ipfs.decodeCid("bafybeiaysi4s6lnjev27ln5icwm6tueaw2vdykrtjkwiphwekaywqhcjze");

            assertTrue(cid.isSupported());
            long time = System.currentTimeMillis();

            Set<Multiaddr> provs = ConcurrentHashMap.newKeySet();
            Set<PeerId> peers = ConcurrentHashMap.newKeySet();

            ipfs.findProviders(session, provs::add, peers::add, cid,
                    new TimeoutCancellable(() -> provs.size() > 10, 30));

            assertFalse(provs.isEmpty());
            for (PeerId peer : peers) {
                LogUtils.info(TAG, "Provider Peer " + peer);
            }
            for (Multiaddr prov : provs) {
                LogUtils.info(TAG, "Provider " + prov);
            }
            LogUtils.debug(TAG, "Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");


            time = System.currentTimeMillis();


            List<Link> res = ipfs.links(session, cid, true, new TimeoutCancellable(60));
            LogUtils.debug(TAG, "Time : " + (System.currentTimeMillis() - time) + " [ms]");
            assertNotNull(res);
            assertFalse(res.isEmpty());

            time = System.currentTimeMillis();
            byte[] content = ipfs.getData(session, cid, new TimeoutProgress(() -> false, 10) {
                @Override
                public void setProgress(int progress) {
                    LogUtils.debug(TAG, "" + progress);
                }

                @Override
                public boolean doProgress() {
                    return true;
                }
            });

            LogUtils.debug(TAG, "Time : " + (System.currentTimeMillis() - time) + " [ms]");

            assertNotNull(content);


            time = System.currentTimeMillis();
            ipfs.removeBlocks(session, cid);
            LogUtils.debug(TAG, "Time : " + (System.currentTimeMillis() - time) + " [ms]");
        }
    }

}