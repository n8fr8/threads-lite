package threads.lite;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsSwarmTest {

    private static final String TAG = IpfsSwarmTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_swarm() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        assertFalse(ipfs.reservations().isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can find our peer
        AtomicBoolean found = new AtomicBoolean(false);
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {
            dummySession.findPeer(new TimeoutCancellable(120), multiaddr -> {
                LogUtils.error(TAG, multiaddr.toString());
                found.set(true);
            }, ipfs.self()); // find me
        }

        assertTrue(found.get());

    }
}
