package threads.lite;


import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManager;

import threads.lite.host.LiteTrust;

@RunWith(AndroidJUnit4.class)
public class IPFSTcpTest {

    private static final String[] protocols = new String[]{"TLSv1.3"};
    private static final String[] cipher_suites = new String[]{"TLS_AES_128_GCM_SHA256"};

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void dummy_test() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        assertNotNull(ipfs);
    }

    //@Test
    public void tcp_test() throws Exception {
        InetSocketAddress address = new InetSocketAddress("147.28.156.11", 4001);


        SSLEngine sslEngine = createSocket(address);
        sslEngine.beginHandshake();

        ByteBuffer encrypted = ByteBuffer.allocate(sslEngine.getSession().getPacketBufferSize());
        sslEngine.wrap(new ByteBuffer[0], encrypted);
        encrypted.flip();
        try (SocketChannel channel = SocketChannel.open(address)) {
            // Send ClientHello, immediately followed by FIN (no TLS Close Alert)
            channel.write(encrypted);
            channel.shutdownOutput();
            // Read ServerHello from server
            encrypted.clear();
            int read = channel.read(encrypted);
            LogUtils.error(TAG, "read " + read);
            encrypted.flip();

            ByteBuffer decrypted = ByteBuffer.allocate(sslEngine.getSession().getApplicationBufferSize());
            sslEngine.unwrap(encrypted, decrypted);
            // It may happen that the read() above read both the ServerHello and the TLS Close Alert.
            if (!encrypted.hasRemaining()) {
                // Now if we can read more, we should read the TLS Close Alert and then the TCP FIN.
                encrypted.clear();
                read = channel.read(encrypted);
                LogUtils.error(TAG, "read " + read);
                encrypted.flip();
            }

            encrypted.clear();

        }


    }

    private static final String TAG = IPFSTcpTest.class.getSimpleName();

    public static SSLEngine createSocket(InetSocketAddress address) throws NoSuchAlgorithmException, KeyManagementException {


        // Get an instance of SSLContext for TLS protocols
        SSLContext sslContext = SSLContext.getInstance("TLS");


        TrustManager[] tm = new TrustManager[1];
        tm[0] = new LiteTrust();
        sslContext.init(null, tm, null);

        // Create the engine
        SSLEngine engine = sslContext.createSSLEngine(address.getHostName(), address.getPort());
        // Use as client
        engine.setUseClientMode(true);
        engine.setHandshakeApplicationProtocolSelector((sslEngine, strings) -> {
            LogUtils.error(TAG, strings.toString());
            return strings.get(0);
        });
        engine.setEnabledProtocols(protocols);
        engine.setEnabledCipherSuites(cipher_suites);
// Get an SSLParameters object from the SSLSocket
        SSLParameters sslp = engine.getSSLParameters();

        // Populate SSLParameters with the ALPN values
        // On the client side the order doesn't matter as
        // when connecting to a JDK server, the server's list takes priority
        String[] clientAPs = {IPFS.ALPN};
        sslp.setApplicationProtocols(clientAPs);


        return engine;
    }

}