package threads.lite.dht;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import threads.lite.LogUtils;
import threads.lite.cid.ID;
import threads.lite.cid.Peer;
import threads.lite.core.Cancellable;

public class Query {

    private static final String TAG = Query.class.getSimpleName();

    private static List<QueryPeer> transform(@NonNull ID key, @NonNull List<Peer> peers) {
        List<QueryPeer> result = new ArrayList<>();
        peers.forEach(peer -> result.add(QueryPeer.create(peer, key)));
        return result;
    }

    public static void runQuery(@NonNull DhtKademlia dht, @NonNull Cancellable cancellable,
                                @NonNull ID key, @NonNull List<QueryPeer> peers,
                                @NonNull DhtKademlia.QueryFunc queryFn) throws InterruptedException {

        int threads = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(threads);
        try {
            QueryPeerSet queryPeers = new QueryPeerSet();
            enhanceSet(dht, queryPeers, peers);

            iteration(dht, executorService, queryPeers, cancellable, key, queryFn, threads);

            boolean termination = executorService.awaitTermination(
                    cancellable.timeout(), TimeUnit.SECONDS);
            LogUtils.info(TAG, "Termination " + termination);
        } finally {
            executorService.shutdown();
            executorService.shutdownNow();
        }
    }

    private static boolean enhanceSet(DhtKademlia dht, QueryPeerSet queryPeers, List<QueryPeer> peers) {
        // the peers in query update are added to the queryPeers
        boolean enhanceSet = false;
        for (QueryPeer peer : peers) {
            if (Objects.equals(peer.getPeer().getPeerId(), dht.session.self())) { // don't add self.
                continue;
            }
            boolean result = queryPeers.add(peer);  // set initial state to PeerHeard
            if (result) {
                enhanceSet = true;
            }
        }
        return enhanceSet;
    }


    public static void iteration(DhtKademlia dht, ExecutorService executorService,
                                 QueryPeerSet queryPeers, Cancellable cancellable, ID key,
                                 DhtKademlia.QueryFunc queryFn, int threads) throws InterruptedException {

        if (cancellable.isCancelled()) {
            executorService.shutdown();
            throw new InterruptedException("operation canceled");
        }

        List<QueryPeer> nextPeersToQuery = queryPeers.nextHeardPeers(threads);

        for (QueryPeer queryPeer : nextPeersToQuery) {
            queryPeer.setState(PeerState.PeerWaiting);
            try {
                if (!executorService.isShutdown()) {
                    executorService.execute(() -> {
                        try {
                            List<Peer> newPeers = queryFn.query(cancellable, queryPeer.getPeer());
                            boolean enhancedSet = enhanceSet(dht, queryPeers,
                                    transform(key, newPeers));
                            queryPeer.setState(PeerState.PeerQueried);
                            // query successful, try to add to routing table
                            if (enhancedSet) {
                                // only when the query peer is returning something
                                // it will be added to the routing table
                                dht.addToRouting(queryPeer);
                            }

                            iteration(dht, executorService, queryPeers, cancellable,
                                    key, queryFn, threads);

                        } catch (InterruptedException ignore) {
                            executorService.shutdown();
                        } catch (ConnectException connectException) {
                            dht.removeFromRouting(queryPeer, false);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } catch (TimeoutException timeoutException) {
                            dht.removeFromRouting(queryPeer, true);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                            dht.removeFromRouting(queryPeer, true);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } finally {
                            if (queryPeers.isDone()) {
                                LogUtils.info(TAG, "Query peers done");
                                executorService.shutdown();
                            }
                        }
                    });
                }
            } catch (RejectedExecutionException ignore) {
                // standard exception
            }
        }

    }

}
