package threads.lite.dht;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.DataHandler;

public class DhtService {

    @NonNull
    static Dht.Message createGetValueMessage(byte[] key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_VALUE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
    }


    @NonNull
    public static Dht.Message createFindNodeMessage(byte[] key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    public static Dht.Message createGetProvidersMessage(Multihash multihash) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(multihash.encoded()))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    public static Dht.Message createPutValueMessage(byte[] target, byte[] value) {
        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());

        Dht.Message.Record rec = Dht.Message.Record.newBuilder()
                .setKey(ByteString.copyFrom(target))
                .setValue(ByteString.copyFrom(value))
                .setTimeReceived(format).build();

        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.PUT_VALUE)
                .setKey(rec.getKey())
                .setRecord(rec)
                .setClusterLevelRaw(0).build();
    }


    @NonNull
    public static Dht.Message createAddProviderMessage(Session session, Multihash multihash) {

        Dht.Message.Builder builder = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.ADD_PROVIDER)
                .setKey(ByteString.copyFrom(multihash.encoded()))
                .setClusterLevelRaw(0);

        Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                .setId(ByteString.copyFrom(session.self().encoded()));
        for (Multiaddr multiaddr : session.publishMultiaddrs()) {
            peerBuilder.addAddrs(ByteString.copyFrom(multiaddr.encoded()));
        }
        builder.addProviderPeers(peerBuilder.build());

        return builder.build();
    }


    public static CompletableFuture<Set<PeerId>> getProvider(Connection connection, Cid cid) {

        CompletableFuture<Set<PeerId>> done = new CompletableFuture<>();

        Dht.Message getProvidersMessage = createGetProvidersMessage(cid.getMultihash());
        request(connection, getProvidersMessage).whenComplete((msg, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                Set<PeerId> peerIds = new HashSet<>();
                List<Dht.Message.Peer> list = msg.getProviderPeersList();
                for (Dht.Message.Peer entry : list) {

                    PeerId peerId = PeerId.create(entry.getId().toByteArray());
                    peerIds.add(peerId);
                }
                done.complete(peerIds);
            }
        });

        return done;

    }


    public static CompletableFuture<Void> provide(Session session, Connection connection, Cid cid) {
        return sendMessage(connection, createAddProviderMessage(session, cid.getMultihash()));
    }

    public static CompletableFuture<Boolean> putValue(
            Connection connection, byte[] target, byte[] value) {

        CompletableFuture<Boolean> done = new CompletableFuture<>();

        // don't allow local users to put bad values.
        try {
            IpnsEntity entry = IpnsService.validate(target, value);
            Objects.requireNonNull(entry);
        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
            return done;
        }

        Dht.Message putValueMessage = createPutValueMessage(target, value);

        request(connection, putValueMessage).whenComplete((msg, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                done.complete(msg.hasRecord());
            }
        });

        return done;
    }

    public static CompletableFuture<Void> sendMessage(Connection connection, Dht.Message message) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        done.completeExceptionally(throwable);
                    }

                    @Override
                    public void streamTerminated(QuicStream quicStream) {
                        if (!done.isDone()) {
                            done.completeExceptionally(new Throwable("stream terminated"));
                        }
                    }

                    @Override
                    public void protocol(Stream stream, String protocol) throws Exception {
                        if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                                IPFS.DHT_PROTOCOL).contains(protocol)) {
                            throw new Exception("Token " + protocol + " not supported");
                        }
                        if (Objects.equals(protocol, IPFS.DHT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message))
                                    .thenApply(Stream::closeOutput)
                                    .thenRun(() -> done.complete(null)); // this is not 100% correct
                        }
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) throws Exception {
                        throw new Exception("data sendMessage invoked");
                    }

                })
                .whenComplete((stream, throwable) -> {
                    if (throwable != null) {
                        done.completeExceptionally(throwable);
                    } else {
                        stream.writeOutput(DataHandler.encodeProtocols(
                                IPFS.MULTISTREAM_PROTOCOL, IPFS.DHT_PROTOCOL));
                    }
                });
        return done;

    }

    public static CompletableFuture<Dht.Message> request(
            Connection connection, Dht.Message message) {

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.DHT_PROTOCOL).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.DHT_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(message))
                            .thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                done.complete(Dht.Message.parseFrom(data.array()));
            }

            public void streamTerminated(QuicStream quicStream) {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }

        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.DHT_PROTOCOL));
            }
        });

        return done;

    }
}
