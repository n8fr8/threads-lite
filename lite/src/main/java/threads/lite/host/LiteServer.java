package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.Version;
import net.luminis.quic.server.ServerConnector;

import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.Server;
import threads.lite.core.StreamHandler;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.server.ServerSession;
import threads.lite.utils.DisabledTrust;
import threads.lite.utils.PackageReader;

public class LiteServer implements Server {
    private static final String TAG = LiteServer.class.getSimpleName();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final ServerSession serverSession;
    @NonNull
    private final LiteCertificate liteCertificate;


    public LiteServer(@NonNull LiteCertificate liteCertificate,
                      @NonNull DatagramSocket socket,
                      @NonNull ServerConnector serverConnector,
                      @NonNull ServerSession serverSession) {
        this.liteCertificate = liteCertificate;
        this.socket = socket;
        this.serverConnector = serverConnector;
        this.serverSession = serverSession;
    }


    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }


    @NonNull
    @Override
    public ServerSession getServerSession() {
        return serverSession;
    }


    @Override
    public void shutdown() {
        serverConnector.shutdown();
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public int numServerConnections() {
        return serverConnector.numConnections();
    }


    @NonNull
    private QuicClientConnection getConnection(StreamHandler streamHandler,
                                               Multiaddr address, Parameters parameters)
            throws ConnectException {

        ConnectionBuilder builder = ConnectionBuilder.newBuilder()
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .remoteAddress(address.getInetSocketAddress())
                .alpn(IPFS.ALPN)
                .transportParams(parameters);

        builder = builder.serverConnector(serverConnector);

        if (parameters.isEnableTrustManager()) {
            builder = builder.trustManager(new LiteTrust(address.getPeerId()));
        } else {
            builder = builder.trustManager(new DisabledTrust());
        }

        if (address.isQuicV1()) {
            builder = builder.version(Version.QUIC_version_1);
        } else {
            builder = builder.version(Version.IETF_draft_29);
        }

        if (address.isAnyLocalAddress()) {
            builder = builder.initialRtt(100);
        }


        return builder.build(quicStream -> new PackageReader(streamHandler));
    }

    @NonNull
    public Connection connect(StreamHandler streamHandler, Multiaddr address,
                              Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;
        PeerId peerId = address.getPeerId();
        QuicClientConnection connection = getConnection(streamHandler, address, parameters);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, peerId);
            connection.connect(IPFS.CONNECT_TIMEOUT);

            run = true;
            return new LiteConnection(connection);
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.warning(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    private void punching(Multiaddr multiaddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = multiaddr.getInetAddress();
            int port = multiaddr.getPort();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            socket.send(datagram);
            LogUtils.error(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(multiaddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    private int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    public void holePunching(@NonNull Multiaddrs multiaddrs) {
        try {
            ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
            for (Multiaddr multiaddr : multiaddrs) {
                service.execute(() -> punching(multiaddr));
            }
            boolean finished = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }
}
