package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.Version;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.Autonat;
import threads.lite.autonat.AutonatService;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.AutonatResult;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Keys;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.Reservation;
import threads.lite.core.Resolver;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.core.SwarmStore;
import threads.lite.crypto.Key;
import threads.lite.dht.DhtKademlia;
import threads.lite.ident.IdentityService;
import threads.lite.ipns.IpnsService;
import threads.lite.minidns.DnsClient;
import threads.lite.minidns.DnsResolver;
import threads.lite.relay.RelayService;
import threads.lite.server.ServerResponder;
import threads.lite.server.ServerSession;
import threads.lite.utils.PackageReader;
import threads.lite.utils.TimeoutCancellable;


public final class LiteHost {
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private final AtomicReference<IPV> version = new AtomicReference<>(IPV.IPv4v6);
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final SwarmStore swarmStore;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final Keys keys;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteCertificate liteCertificate;
    @NonNull
    private final AtomicReference<Multiaddr> dialableAddress = new AtomicReference<>();
    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();

    @NonNull
    private final DnsClient dnsClient;

    @NonNull
    private final Resolver resolver;

    @NonNull
    private final Crypto.PublicKey publicKey;
    @Nullable
    private Consumer<LitePush> incomingPush;

    @Nullable
    private Supplier<IpnsRecord> recordSupplier;

    public LiteHost(@NonNull Keys keys,
                    @NonNull BlockStore blockStore,
                    @NonNull PeerStore peerStore,
                    @NonNull SwarmStore swarmStore)
            throws Exception {

        this.liteCertificate = LiteCertificate.createCertificate(keys);
        this.keys = keys;
        this.blockStore = blockStore;
        this.peerStore = peerStore;
        this.swarmStore = swarmStore;

        this.self = Key.createPeerId(keys.getPublic());
        evaluateProtocol();


        this.dnsClient = DnsResolver.getInstance(() -> {
            switch (version.get()) {
                case IPv4:
                    return new ArrayList<>(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                case IPv6:
                    return new ArrayList<>(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                default:
                    ArrayList<InetAddress> list = new ArrayList<>();
                    list.addAll(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                    list.addAll(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                    return list;
            }
        });

        this.publicKey = Key.createCryptoKey(keys.getPublic());
        this.resolver = new Resolver(dnsClient, ipv());

    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    public DnsClient getDnsClient() {
        return dnsClient;
    }

    public void setRecordSupplier(@Nullable Supplier<IpnsRecord> recordSupplier) {
        this.recordSupplier = recordSupplier;
    }

    @NonNull
    private DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }


    @NonNull
    public Keys getKeys() {
        return keys;
    }


    @NonNull
    public Set<Reservation> reservations() {
        return reservations;
    }


    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 boolean findProvidersActive) {
        return new LiteSession(blockStore, this, findProvidersActive);
    }

    @NonNull
    public PeerId self() {
        return self;
    }


    @NonNull
    public BlockStore getBlockStore() {
        return blockStore;
    }


    @NonNull
    public Set<Multiaddr> networkListenAddresses(Server server) {
        Set<Multiaddr> set = new HashSet<>();

        int port = server.getPort();
        try {
            for (InetAddress inetAddress : Network.networkAddresses()) {
                try {
                    set.add(Multiaddr.create(Version.QUIC_version_1,
                            self, new InetSocketAddress(inetAddress, port)));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return set;
    }


    @NonNull
    public Multiaddrs publishMultiaddrs(int maxReservations) {
        Multiaddrs set = new Multiaddrs();
        Multiaddr dialable = dialableAddress.get();
        if (dialable != null) {
            set.add(dialable);
        }
        for (Reservation reservation : reservations) {
            try {
                if (reservation.getConnection().isConnected()) {
                    set.addAll(reservation.getAddresses());
                    if (set.size() > maxReservations) {
                        break;
                    }
                } else {
                    reservations.remove(reservation);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return set;
    }


    @NonNull
    public Set<Peer> getRoutingPeers() {
        Set<Peer> peers = new HashSet<>();
        Set<Multiaddr> bootstrap = getBootstrap();

        Set<PeerId> peerIds = new HashSet<>();
        for (String name : new HashSet<>(IPFS.DHT_BOOTSTRAP_NODES)) {
            try {
                PeerId peerId = PeerId.decode(name);
                Objects.requireNonNull(peerId);
                peerIds.add(peerId);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        for (Multiaddr addr : bootstrap) {
            for (PeerId peerId : peerIds) {
                try {
                    PeerId cmpPeerId = addr.getPeerId();
                    if (Objects.equals(cmpPeerId, peerId)) {
                        Peer peer = Peer.create(addr);
                        peer.setReplaceable(false);
                        peers.add(peer);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        }
        return peers;
    }

    public void push(@NonNull Connection connection, @NonNull IpnsEntity ipnsEntity) {
        try {
            if (incomingPush != null) {
                incomingPush.accept(new LitePush(connection, ipnsEntity));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<LitePush> incomingPush) {
        this.incomingPush = incomingPush;
    }


    public void updateNetwork() {
        evaluateProtocol();
    }

    private void evaluateProtocol() {
        List<InetAddress> addresses = new ArrayList<>();
        try {
            addresses.addAll(Network.networkAddresses());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        if (!addresses.isEmpty()) {
            version.set(getProtocol(addresses));
        } else {
            version.set(IPV.IPv4);
        }
    }


    @NonNull
    private IPV getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return IPV.IPv6; // prefer ipv6
        } else if (ipv4) {
            return IPV.IPv4;
        } else if (ipv6) {
            return IPV.IPv6;
        } else {
            return IPV.IPv4v6;
        }
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        return IdentityService.getPeerInfo(self(), conn);
    }

    @NonNull
    public PeerInfo getIdentity() throws Exception {
        IdentifyOuterClass.Identify identity = IdentityService.createIdentity(publicKey,
                Set.of(IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL,
                        IPFS.LITE_PUSH_PROTOCOL, IPFS.BITSWAP_PROTOCOL,
                        IPFS.IDENTITY_PROTOCOL, IPFS.DHT_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP),
                publishMultiaddrs(IPFS.MAX_PUBLISH_RESERVATIONS), null);
        return IdentityService.getPeerInfo(self(), identity);

    }


    @NonNull
    public AutonatResult autonat(@NonNull Server server) {
        autonat.lock();
        try (Session session = createSession(blockStore, false)) {
            Autonat autonat = new Autonat();
            Set<Multiaddr> addresses = networkListenAddresses(server);
            AutonatService.autonat(session, addresses, autonat, getBootstrap());

            Multiaddr winner = autonat.winner();
            if (winner != null) {
                dialableAddress.set(winner);
            }

            return new AutonatResult(autonat.getNatType(), winner);
        } finally {
            autonat.unlock();
        }
    }


    @NonNull
    public Set<Reservation> reservations(@NonNull Server server,
                                         @NonNull Collection<Multiaddr> multiaddrs,
                                         @NonNull Cancellable cancellable) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = ConcurrentHashMap.newKeySet();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }

                if (reservation.isStaticRelay()) {
                    // still valid
                    relaysStillValid.add(reservation.getRelayId());
                } else {
                    if (reservation.expireInMinutes() > 2) {
                        relaysStillValid.add(reservation.getRelayId());
                    } else {
                        list.remove(reservation);
                    }
                }
            }

            if (!multiaddrs.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : multiaddrs) {
                    service.execute(() -> {
                        try {
                            if (cancellable.isCancelled()) {
                                return;
                            }
                            PeerId peerId = address.getPeerId();
                            Objects.requireNonNull(peerId);

                            if (!relaysStillValid.contains(peerId)) {
                                reservation(server, address).get(
                                        cancellable.timeout(), TimeUnit.SECONDS);
                            } // else case: nothing to do here, reservation is sill valid

                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable.getMessage());
                        }
                    });

                }
                service.shutdown();
                if (cancellable.timeout() > 0) {
                    try {
                        boolean termination = service.awaitTermination(
                                cancellable.timeout(), TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    public CompletableFuture<Reservation> reservation(@NonNull Server server,
                                                      @NonNull Multiaddr multiaddr) {
        CompletableFuture<Reservation> done = new CompletableFuture<>();
        RelayService.reservation(this, server, multiaddr)
                .whenComplete((reservation, throwable) -> {
                            if (throwable != null) {
                                done.completeExceptionally(throwable);
                            } else {
                                reservations.add(reservation);
                                done.complete(reservation);
                            }
                        }
                );
        return done;
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public Set<Multiaddr> getBootstrap() {

        Set<Multiaddr> multiaddrs = DnsResolver.resolveDnsaddrHost(getDnsClient(), ipv().get(),
                IPFS.LIB2P_DNS);
        if (ipv().get() == IPV.IPv4 || ipv().get() == IPV.IPv4v6) {
            try {
                multiaddrs.add(Multiaddr.create(IPFS.KAD_BOOTSTRAP));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return multiaddrs;
    }


    @NonNull
    public Supplier<IPV> ipv() {
        return version::get;
    }

    // creates a self signed record (used for ipns)
    public byte[] createSelfSignedRecord(byte[] value, long sequence) throws Exception {
        Date eol = Date.from(new Date().toInstant().plus(IPFS.RECORD_EOL));

        Duration duration = Duration.ofHours(IPFS.IPNS_DURATION);
        ipns.pb.Ipns.IpnsEntry record =
                IpnsService.create(keys.getPrivate(), value, sequence, eol, duration);

        record = IpnsService.embedPublicKey(keys.getPublic(), record);

        return record.toByteArray();
    }


    @NonNull
    public PeerStore getPeerStore() {
        return peerStore;
    }

    @NonNull
    public IpnsRecord getIpnsRecord() throws Exception {
        if (recordSupplier != null) {
            return recordSupplier.get();
        }
        // return empty content
        return new IpnsRecord(Key.createIpnsKey(self()),
                createSelfSignedRecord(new byte[0], 0));
    }


    @NonNull
    public LiteServer createServer(DatagramSocket socket,
                                   @NonNull Consumer<Connection> connectConsumer,
                                   @NonNull Consumer<Connection> closedConsumer,
                                   @NonNull Function<PeerId, Boolean> isGated) {

        ServerSession serverSession = new ServerSession(this);

        ServerConnector server = createServerConnector(socket, new ServerResponder(serverSession));

        server.setClosedConsumer(connection -> closedConsumer.accept(new LiteConnection(connection)));

        server.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
            @Override
            public void createConnection(String protocol, QuicConnection quicConnection) {

                LogUtils.error(TAG, "Server connection established " +
                        quicConnection.getRemoteAddress().toString());

                LiteConnection liteConnection = new LiteConnection(quicConnection);

                try {
                    X509Certificate cert = quicConnection.getRemoteCertificate();
                    Objects.requireNonNull(cert);
                    PeerId remotePeerId = LiteCertificate.extractPeerId(cert);
                    Objects.requireNonNull(remotePeerId);

                    if (isGated.apply(remotePeerId)) {
                        throw new Exception("Peer is gated " + remotePeerId);
                    }

                    // now the remote PeerId is available
                    quicConnection.setAttribute(Connection.REMOTE_PEER, remotePeerId);

                } catch (Throwable throwable) {
                    quicConnection.close();
                    LogUtils.error(TAG, throwable);
                    return;
                }

                try {
                    connectConsumer.accept(liteConnection);
                } catch (Throwable ignore) {
                }

            }
        });

        server.start();
        return new LiteServer(liteCertificate, socket, server, serverSession);
    }

    @NonNull
    private ServerConnector createServerConnector(DatagramSocket socket, StreamHandler streamHandler) {
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.QUIC_version_1);
        supportedVersions.add(Version.QUIC_version_2);
        return new ServerConnector(socket, new LiteTrust(), liteCertificate.certificate(),
                liteCertificate.privateKey(), supportedVersions,
                quicStream -> new PackageReader(streamHandler),
                false, false);

    }


    public Server startSever(int port,
                             @NonNull Consumer<Connection> connectConsumer,
                             @NonNull Consumer<Connection> closedConsumer,
                             @NonNull Function<PeerId, Boolean> isGated) {

        DatagramSocket socket = getSocket(port);

        return createServer(socket, connectConsumer, closedConsumer, isGated);

    }


    @NonNull
    public LiteCertificate getLiteCertificate() {
        return liteCertificate;
    }

    @NonNull
    public Resolver getResolver() {
        return resolver;
    }

    @NonNull
    public Crypto.PublicKey getPublicKey() {
        return publicKey;
    }


    public void reservations(Server server, TimeoutCancellable timeoutCancellable) {
        try {

            List<Multiaddr> swarm = swarmStore.getMultiaddrs();

            Set<Reservation> reservations = reservations(
                    server, swarm, timeoutCancellable);

            // cleanup swarm begin
            Set<Multiaddr> relays = ConcurrentHashMap.newKeySet();
            for (Reservation reservation : reservations) {
                LogUtils.error(TAG, reservation.toString());
                relays.add(reservation.getRelayAddress());
            }
            for (Multiaddr address : swarm) {
                if (!relays.contains(address)) {
                    swarmStore.removeMultiaddr(address);
                }
            }
            // cleanup swarm end

            if (timeoutCancellable.isCancelled()) {
                return;
            }

            DhtKademlia dhtKademlia = new DhtKademlia(server.getServerSession());


            // fill up reservations [not yet enough]
            dhtKademlia.findClosestPeers(timeoutCancellable,
                    multiaddr -> {

                        if (relays.contains(multiaddr)) {
                            return;
                        }

                        if (timeoutCancellable.isCancelled()) {
                            return;
                        }

                        reservation(server, multiaddr)
                                .whenComplete((reservation, throwable) -> {
                                    if (throwable != null) {
                                        LogUtils.error(TAG, throwable.getMessage());
                                    } else {
                                        swarmStore.storeMultiaddr(reservation.getRelayAddress());
                                    }
                                });
                    }, self());


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}


