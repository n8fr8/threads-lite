package threads.lite.core;

import net.luminis.quic.TransportParameters;

import threads.lite.IPFS;

public class Parameters extends TransportParameters {

    public boolean isEnableTrustManager() {
        return enableTrustManager;
    }

    public void setEnableTrustManager(boolean enableTrustManager) {
        this.enableTrustManager = enableTrustManager;
    }

    private boolean enableTrustManager = true;

    public Parameters(int maxIdleTimeoutInSeconds,
                      int initialMaxStreamData,
                      int initialMaxStreamsBidirectional) {
        super(maxIdleTimeoutInSeconds, initialMaxStreamData,
                initialMaxStreamsBidirectional, 0);

    }

    public static Parameters getDefaultUniDirection(int maxIdleTimeoutInSeconds,
                                                    boolean enableTrustManager) {
        Parameters parameters = new Parameters(maxIdleTimeoutInSeconds, IPFS.MESSAGE_SIZE_MAX,
                0);
        parameters.setEnableTrustManager(enableTrustManager);
        return parameters;
    }

    public static Parameters getDefault() {
        return new Parameters(IPFS.GRACE_PERIOD, IPFS.MESSAGE_SIZE_MAX, IPFS.MAX_STREAMS);
    }
}
