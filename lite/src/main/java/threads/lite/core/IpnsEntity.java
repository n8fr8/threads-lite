package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.Date;

import threads.lite.cid.PeerId;

public class IpnsEntity {

    @NonNull
    private final PeerId peerId;
    @NonNull
    private final String type;

    @NonNull
    public String getType() {
        return type;
    }

    private final long sequence;
    private final byte[] value;
    @NonNull
    private final Date eol;


    public IpnsEntity(@NonNull PeerId peerId, @NonNull String type, @NonNull Date eol,
                      byte[] value, long sequence) {
        this.peerId = peerId;
        this.type = type;
        this.eol = eol;
        this.sequence = sequence;
        this.value = value;
    }

    @NonNull
    @Override
    public String toString() {
        return "IpnsEntity{" +
                "peerId=" + peerId +
                ", type='" + type + '\'' +
                ", sequence=" + sequence +
                ", eol=" + eol +
                '}';
    }

    @NonNull
    public Date getEol() {
        return eol;
    }

    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    public long getSequence() {
        return sequence;
    }

    public byte[] getValue() {
        return value;
    }
}