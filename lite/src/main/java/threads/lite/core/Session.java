package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.Version;

import java.io.Closeable;
import java.net.ConnectException;
import java.net.DatagramSocket;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import crypto.pb.Crypto;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.host.LiteCertificate;
import threads.lite.host.LiteConnection;
import threads.lite.host.LiteServer;
import threads.lite.host.LiteTrust;
import threads.lite.quic.ConnectionBuilder;
import threads.lite.relay.RelayConnection;
import threads.lite.relay.RelayService;
import threads.lite.utils.DisabledTrust;
import threads.lite.utils.PackageReader;


public abstract class Session implements Closeable, Routing, BitSwap {

    private static final String TAG = Session.class.getSimpleName();
    @NonNull
    protected final Set<Connection> swarm = ConcurrentHashMap.newKeySet();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);

    // returns all supported protocols within the session
    @NonNull
    abstract public Map<String, ProtocolHandler> getProtocols();

    @NonNull
    abstract public LiteCertificate getLiteCertificate();

    // initial routing peers for the DHT
    @NonNull
    abstract public Set<Peer> getRoutingPeers();

    @NonNull
    public Set<String> getProtocolNames() {
        return new HashSet<>(getProtocols().keySet());
    }

    @Nullable
    public ProtocolHandler getProtocolHandler(@NonNull String protocol) {
        return getProtocols().get(protocol);
    }

    // add a protocol handler to the supported list of protocols.
    // Note: a protocol handler is only invoked, when a remote peer initiate a
    // stream over an existing connection
    abstract public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception;

    // the stream handler which is invoked on peer initiated streams
    @NonNull
    abstract public StreamHandler getStreamHandler();

    @NonNull
    abstract public Resolver getResolver();

    // returns the block store where all data is stored
    @NonNull
    abstract public BlockStore getBlockStore();

    @NonNull
    abstract public PeerStore getPeerStore();

    // returns true, when in bitswap the provider search is enabled
    abstract public boolean isFindProvidersActive();

    abstract public PeerId self();

    // returns all connections of the swarm
    @NonNull
    public Set<Connection> getSwarm() {
        return swarm;
    }

    // returns true when session is closed, otherwise false (note: when session is closed
    // operations on a session will fail [Interrupt Exception]
    abstract public boolean isClosed();


    @NonNull
    public Connection dial(Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {

        Multiaddr resolved = getResolver().resolveToOne(address);
        if (resolved == null) {
            throw new ConnectException("no addresses left");
        }

        if (address.isCircuitAddress()) {
            try {
                RelayConnection relayConnection = RelayService.createRelayConnection(
                        this, address, parameters);
                return relayConnection.upgradeConnection();
            } catch (InterruptedException | ConnectException | TimeoutException exception) {
                throw exception;
            } catch (Throwable throwable) {
                throw new ConnectException(throwable.getMessage());
            }
        } else {
            return connect(address, parameters);
        }
    }

    @NonNull
    public LiteConnection connect(Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;


        QuicClientConnection connection = getConnection(address, parameters);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, address.getPeerId());
            connection.connect(IPFS.CONNECT_TIMEOUT);
            run = true;
            LiteConnection liteConnection = new LiteConnection(connection);
            swarm.add(liteConnection);
            return liteConnection;
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }


    @NonNull
    private QuicClientConnection getConnection(Multiaddr address, Parameters parameters)
            throws ConnectException {

        try {
            LiteCertificate liteCertificate = getLiteCertificate();
            Objects.requireNonNull(liteCertificate);
            ConnectionBuilder builder = ConnectionBuilder.newBuilder()
                    .clientCertificate(liteCertificate.x509Certificate())
                    .clientCertificateKey(liteCertificate.getKey())
                    .remoteAddress(address.getInetSocketAddress())
                    .alpn(IPFS.ALPN)
                    .transportParams(parameters);

            if (parameters.isEnableTrustManager()) {
                builder = builder.trustManager(new LiteTrust(address.getPeerId()));
            } else {
                builder = builder.trustManager(new DisabledTrust());
            }

            if (address.isQuicV1()) {
                builder = builder.version(Version.QUIC_version_1);
            } else {
                builder = builder.version(Version.IETF_draft_29);
            }

            if (address.isAnyLocalAddress()) {
                builder = builder.initialRtt(100);
            }


            return builder.build(quicStream -> new PackageReader(getStreamHandler()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw throwable;
        }
    }


    abstract public Set<Multiaddr> publishMultiaddrs();

    abstract public Crypto.PublicKey getPublicKey();

    abstract public Keys getKeys();

    abstract public LiteServer createServer(@NonNull DatagramSocket socket,
                                            @NonNull Consumer<Connection> connectConsumer,
                                            @NonNull Consumer<Connection> closedConsumer,
                                            @NonNull Function<PeerId, Boolean> isGated);
}

