package threads.lite.core;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.util.concurrent.TimeoutException;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.server.ServerSession;

public interface Server {
    void shutdown();

    int getPort();

    int numServerConnections();

    @NonNull
    ServerSession getServerSession();

    @NonNull
    DatagramSocket getSocket();

    @NonNull
    Connection connect(StreamHandler streamHandler, Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException;

    void holePunching(@NonNull Multiaddrs multiaddrs);
}
