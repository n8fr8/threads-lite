package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;

public interface PeerStore {

    List<Peer> getRandomPeers(int limit);

    void storeMultiaddr(@NonNull Multiaddr multiaddr);

    void removeMultiaddr(@NonNull Multiaddr multiaddr);

    void storePeer(@NonNull Peer peer);

    @Nullable
    Multiaddr getMultiaddr(@NonNull PeerId peerId);

    void removePeer(Peer peer);
}
