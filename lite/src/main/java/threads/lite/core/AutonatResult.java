package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.cid.Multiaddr;

public class AutonatResult {
    @NonNull
    private final NatType natType;
    @Nullable
    private final Multiaddr dialableAddress;

    public AutonatResult(@NonNull NatType natType, @Nullable Multiaddr dialableAddress) {
        this.natType = natType;
        this.dialableAddress = dialableAddress;
    }

    // returns NatType (SYMMETRIC, FULL_CONE, RESTRICTED_CONE,
    // PORT_RESTRICTED_CONE, UNKNOWN), only matters when there is no
    // dialable address
    // Note: right now it does not yet distinguish between FULL_CONE, RESTRICTED_CONE,
    // and PORT_RESTRICTED_CONE -> it returns always RESTRICTED_CONE
    @NonNull
    @Override
    public String toString() {
        return "Success " + success() +
                " Address " + dialableAddress() +
                " NatType " + getNatType();
    }

    @NonNull
    public NatType getNatType() {
        return natType;
    }

    @Nullable
    public Multiaddr dialableAddress() {
        return dialableAddress;
    }

    public boolean success() {
        return dialableAddress != null;
    }

    // FULL_CONE, RESTRICTED_CONE, PORT_RESTRICTED_CONE is RESTRICTED_CONE
    // do not know how to distinguish them
    public enum NatType {
        SYMMETRIC, RESTRICTED_CONE, UNKNOWN
    }

}
