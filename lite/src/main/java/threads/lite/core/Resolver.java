package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Set;
import java.util.function.Supplier;

import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.minidns.DnsClient;
import threads.lite.minidns.DnsResolver;

public class Resolver {

    private static final int TIMEOUT = 100;
    private static final String TAG = Resolver.class.getSimpleName();

    private final Supplier<IPV> ipv;
    private final DnsClient dnsClient;

    public Resolver(DnsClient dnsClient, Supplier<IPV> ipv) {
        this.dnsClient = dnsClient;
        this.ipv = ipv;
    }

    @Nullable
    public Multiaddr resolveToOne(Multiaddr multiaddr) {
        Multiaddrs resolvedAddrs = new Multiaddrs();

        resolvedAddrs.addAll(resolveAddress(multiaddr));

        return resolved(resolvedAddrs);
    }

    @Nullable
    public Multiaddr resolveToOne(Multiaddrs multiaddrs) {
        if (multiaddrs.isEmpty()) {
            return null;
        }
        Multiaddrs resolvedAddrs = new Multiaddrs();
        for (Multiaddr multiaddr : multiaddrs) {
            resolvedAddrs.addAll(resolveAddress(multiaddr));
        }
        return resolved(resolvedAddrs);
    }

    @Nullable
    private Multiaddr resolved(Multiaddrs resolvedAddrs) {
        if (resolvedAddrs.isEmpty()) {
            return null;
        }
        if (resolvedAddrs.size() == 1) {
            // nothing to do anymore [for now] todo
            return resolvedAddrs.iterator().next();
        }
        for (Multiaddr resolvedAddr : resolvedAddrs) {
            if (isReachable(resolvedAddr)) {
                return resolvedAddr;
            }
        }
        return null;
    }


    private boolean isReachable(Multiaddr multiaddr) {
        try {
            return multiaddr.getInetAddress().isReachable(TIMEOUT);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, multiaddr.toString() + " " + throwable.getMessage());
        }
        return false;
    }

    @NonNull
    private Set<Multiaddr> resolveAddress(@NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns() || multiaddr.isDns6() || multiaddr.isDns4()) {
            Multiaddr resolved = DnsResolver.resolveDns(dnsClient, multiaddr);
            if (resolved != null) {
                return Set.of(resolved);
            }
        } else if (multiaddr.isDnsaddr()) {
            return DnsResolver.resolveDnsaddr(dnsClient, ipv.get(), multiaddr);
        } else {
            return Set.of(multiaddr);
        }
        return Set.of();
    }

    public Supplier<IPV> ipv() {
        return ipv;
    }
}
