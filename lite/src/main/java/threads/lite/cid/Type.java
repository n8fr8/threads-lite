package threads.lite.cid;

public class Type {
    public static final int IP4 = 4;
    public static final int IP6 = 41;
    public static final int UDP = 273;
    public static final int P2P = 421;
    public static final int P2PCIRCUIT = 290;
    public static final int DNSADDR = 56;

    public static final int DNS = 53;
    public static final int DNS4 = 54;
    public static final int DNS6 = 55;
    public static final int QUIC = 460;
    public static final int QUICV1 = 461;

}
