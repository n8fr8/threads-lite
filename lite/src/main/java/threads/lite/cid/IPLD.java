package threads.lite.cid;

import android.util.SparseArray;

import androidx.annotation.NonNull;

public enum IPLD {
    IDENTITY(0x00),
    RAW(0x55),
    DagProtobuf(0x70),
    Libp2pKey(0x72);

    public int getCodec() {
        return codec;
    }

    private final int codec;

    IPLD(int codec) {
        this.codec = codec;
    }

    private static final SparseArray<IPLD> byCode = new SparseArray<>();

    static {
        for (IPLD t : IPLD.values()) {
            byCode.put(t.getCodec(), t);
        }
    }

    @NonNull
    public static IPLD get(int codec) {
        IPLD ipld = byCode.get(codec);
        if (ipld == null) {
            throw new IllegalStateException("No IPLD with codec: " + codec);
        }
        return ipld;
    }

}

