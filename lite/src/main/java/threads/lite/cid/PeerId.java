package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import java.nio.ByteBuffer;
import java.util.Arrays;

import threads.lite.utils.DataHandler;

public final class PeerId implements Tag, Comparable<PeerId> {

    private final byte[] bytes;

    private PeerId(byte[] bytes) {
        this.bytes = bytes;
    }

    @NonNull
    @TypeConverter
    public static PeerId fromArray(byte[] data) {
        return PeerId.create(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(PeerId peerId) {
        return peerId.encoded();
    }

    @NonNull
    public static PeerId create(byte[] bytes) {
        return new PeerId(bytes);
    }

    @NonNull
    public static PeerId decode(@NonNull String name) throws Exception {

        if (name.startsWith("Qm") || name.startsWith("1")) {
            // base58 encoded sha256 or identity multihash
            return PeerId.create(Base58.decode(name));
        }

        byte[] data = Multibase.decode(name);

        ByteBuffer wrap = ByteBuffer.wrap(data);
        DataHandler.readUnsignedVariant(wrap); // skip version
        DataHandler.readUnsignedVariant(wrap); // skip codec
        Multihash mh = Multihash.decode(wrap);
        return PeerId.create(mh.encoded());
    }


    // https://docs.ipfs.tech/concepts/content-addressing/#cid-inspector
    // The default for CIDv1 is the case-insensitive base32, but use of the shorter base36 is
    // encouraged for IPNS names to ensure same text representation on subdomains.
    //
    @NonNull
    public String toBase36() {
        Multihash multihash = Multihash.decode(bytes);
        return Multibase.encode(Multibase.Base.Base36,
                Cid.createCidV1(IPLD.Libp2pKey, multihash).encoded());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerId peer = (PeerId) o;
        return Arrays.equals(bytes, peer.bytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }


    // https://github.com/libp2p/specs/blob/master/peer-ids/peer-ids.md
    // String representation
    // There are two ways to represent peer IDs in text: as a raw base58btc encoded multihash
    // (e.g., Qm..., 1...) and as a multibase encoded CID (e.g., bafz...). Libp2p is slowly
    // transitioning from the first (legacy) format to the second (new).
    //
    // Implementations MUST support parsing both forms of peer IDs. Implementations SHOULD display
    // peer IDs using the first (raw base58btc encoded multihash) format until the second format
    // is widely supported.
    //
    // Peer IDs encoded as CIDs must be encoded using CIDv1 and must use the libp2p-key multicodec
    // (0x72). By default, such peer IDs SHOULD be encoded in using the base32 multibase
    // (RFC4648, without padding).
    //
    // For reference, CIDs (encoded in text) have the following format
    //
    // <multibase-prefix><cid-version><multicodec><multihash>
    // [TODO future toBase32() instead of toBase58()]
    @NonNull
    public String toString() {
        return Base58.encode(this.bytes);
    }

    @NonNull
    public byte[] encoded() {
        return this.bytes;
    }

    @Override
    public int compareTo(PeerId o) {
        return DataHandler.compareUnsigned(this.bytes, o.bytes);
    }

}
