package threads.lite.autonat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import threads.lite.cid.Multiaddr;
import threads.lite.core.AutonatResult;


// NAT types defined in STUN
// Full-cone NAT
// All requests from the same private IP address and port (for example, IP1:Port1) are mapped
// to the same public IP address and port (for example, IP:Port). In addition, any host on
// the Internet can communicate with the host on the intranet by sending packets to the mapped
// public IP address and port.
//
// This is a relatively loose NAT policy. As long as the mapping between the private
// IP address and port and the public IP address and port is established, any host on the
// Internet can access the host on the intranet through the NAT device.
//
// Restricted-cone NAT
// All requests from the same private IP address and port (for example, IP1:Port1) are
// mapped to the same public IP address and port (for example, IP:Port). A host on the
// Internet can send packets to the host on the intranet only if the host on the intranet
// has previously sent a packet to the host on the Internet.
//
//
// Port-restricted cone NAT
// Port-restricted cone NAT is similar to restricted-cone NAT, but the restriction includes port
// numbers. That is, a host on the Internet (for example, IP2:Port2) can send packets to a
// host on the intranet only if the host on the intranet has previously sent a packet to
// the host on the Internet.
//
// Symmetric NAT
// All requests sent from the same private IP address and port to a specific destination IP
// address and port are mapped to the same IP address and port. If a host sends a packet with
// the same source IP address and port number to a different destination, a different NAT mapping
// is used. In addition, only a host on the Internet that receives a packet from a host on the
// intranet can send a packet back.
//
// Unlike port-restricted cone NAT that maps all requests from the same private IP address
// and port to the same public IP address and port, regardless of their destinations, symmetric
// NAT maps requests with the same source IP address and port number but different
// destinations to different public IP addresses and ports.
public class Autonat extends ConcurrentHashMap<Multiaddr, Integer> {

    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    private final AtomicBoolean abort = new AtomicBoolean(false);
    private AutonatResult.NatType natType = AutonatResult.NatType.UNKNOWN;
    private String host;


    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean abort() {
        return abort.get();
    }

    public void addAddr(Multiaddr multiaddr) {

        Integer match = get(multiaddr);
        if (match == null) {
            put(multiaddr, 1);
        } else {
            int incMatch = ++match;
            put(multiaddr, incMatch);
        }
    }

    @NonNull
    public AutonatResult.NatType getNatType() {
        return natType;
    }

    @Nullable
    public Multiaddr winner() {
        Map.Entry<Multiaddr, Integer> winner = null;
        for (Map.Entry<Multiaddr, Integer> entry : this.entrySet()) {
            if (winner == null) {
                winner = entry;
            } else {
                if (winner.getValue() < entry.getValue()) {
                    winner = entry;
                }
            }
        }

        // at least the winner should be have 3 matched
        if (winner != null) {
            if (winner.getValue() >= 3) {
                return winner.getKey();
            }
        }
        return null;
    }

    public void evaluateNatType(Multiaddr observed, InetSocketAddress local) {
        autonat.lock();

        try {
            // Note: It is assumed that the calling autonat nodes are
            // not lying (otherwise, more has to be done)
            // The autonat nodes are default IPFS nodes

            if (host == null) {
                host = observed.getHost();
            }

            if (!Objects.equals(host, observed.getHost())) {
                // we can abort here, minimum ist that the hosts are equal
                natType = AutonatResult.NatType.SYMMETRIC; // this is not correct, but in general
                abort.set(true);
                // it means no punch-hole
                return;
            }

            if (local.getPort() != observed.getPort()) {
                // probably we can abort here
                natType = AutonatResult.NatType.SYMMETRIC; // ports are not equal of at least
                abort.set(true);
                // two autonat calls
            } else {
                // ports are equal, now it can be any of the remaining three
                natType = AutonatResult.NatType.RESTRICTED_CONE; // not right, more has to be done
            }

        } finally {
            autonat.unlock();
        }
    }
}
