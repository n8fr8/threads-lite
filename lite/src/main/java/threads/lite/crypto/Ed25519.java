package threads.lite.crypto;

import androidx.annotation.NonNull;

import com.google.crypto.tink.subtle.Ed25519Verify;

import java.util.Arrays;

import crypto.pb.Crypto;


public class Ed25519 {

    public static PubKey unmarshalEd25519PublicKey(byte[] keyBytes) {
        return new Ed25519PublicKey(keyBytes);
    }

    public static final class Ed25519PublicKey extends PubKey {
        private final byte[] publicKey;

        public Ed25519PublicKey(byte[] publicKey) {
            super(Crypto.KeyType.Ed25519);
            this.publicKey = publicKey;
        }


        @NonNull
        public byte[] raw() {
            return this.publicKey;
        }

        public void verify(byte[] data, byte[] signature) throws Exception {
            Ed25519Verify verifier = new Ed25519Verify(publicKey);
            verifier.verify(signature, data);
        }

        public int hashCode() {
            return Arrays.hashCode(this.publicKey);
        }
    }

}
