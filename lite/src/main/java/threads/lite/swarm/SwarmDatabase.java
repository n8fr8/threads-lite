package threads.lite.swarm;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.Multiaddr;

@androidx.room.Database(entities = {Multiaddr.class}, version = 1, exportSchema = false)
@TypeConverters({Multiaddr.class})
public abstract class SwarmDatabase extends RoomDatabase {

    public abstract SwarmDao swarmDao();

}
