package threads.lite.quic;

import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;
import net.luminis.quic.server.ServerConnector;
import net.luminis.tls.CipherSuite;

import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

public class ConnectionBuilder {
    private InetSocketAddress remoteAddress;
    private Version quicVersion;
    private Integer initialRtt;
    private X509TrustManager trustManager;
    private X509Certificate clientCertificate;
    private PrivateKey clientCertificateKey;
    private TransportParameters transportParams;
    private String alpn;
    private ServerConnector serverConnector;

    public static ConnectionBuilder newBuilder() {
        return new ConnectionBuilder();
    }

    public QuicClientConnectionImpl build(
            Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) throws ConnectException {

        if (remoteAddress == null) {
            throw new IllegalStateException("Cannot create connection when address is not set");
        }
        if (initialRtt != null && initialRtt < 1) {
            throw new IllegalArgumentException("Initial RTT must be larger than 0.");
        }
        if (transportParams == null) {
            throw new IllegalStateException("Connection requires transport parameters");
        }
        if (alpn == null) {
            throw new IllegalStateException("Connection requires application protocol");
        }
        if (quicVersion == null) {
            throw new IllegalStateException("Connection requires quic version");
        }
        try {
            QuicClientConnectionImpl quicConnection =
                    new QuicClientConnectionImpl(alpn, remoteAddress, quicVersion,
                            initialRtt, List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                            clientCertificate, clientCertificateKey, transportParams,
                            serverConnector, streamDataConsumer);

            if (trustManager != null) {
                quicConnection.trustManager(trustManager);
            }


            return quicConnection;

        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }

    }

    public ConnectionBuilder alpn(String alpn) {
        this.alpn = alpn;
        return this;
    }

    public ConnectionBuilder serverConnector(ServerConnector server) {
        this.serverConnector = server;
        return this;
    }

    public ConnectionBuilder transportParams(TransportParameters transportParams) {
        this.transportParams = transportParams;
        return this;
    }

    public ConnectionBuilder version(Version version) {
        quicVersion = version;
        return this;
    }

    public ConnectionBuilder remoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }


    public ConnectionBuilder initialRtt(int initialRtt) {
        this.initialRtt = initialRtt;
        return this;
    }

    public ConnectionBuilder trustManager(X509TrustManager trustManager) {
        this.trustManager = trustManager;
        return this;
    }

    public ConnectionBuilder clientCertificate(X509Certificate certificate) {
        this.clientCertificate = certificate;
        return this;
    }

    public ConnectionBuilder clientCertificateKey(PrivateKey privateKey) {
        this.clientCertificateKey = privateKey;
        return this;
    }

}
