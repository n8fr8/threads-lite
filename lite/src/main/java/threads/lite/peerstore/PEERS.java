package threads.lite.peerstore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.PeerStore;


public class PEERS implements PeerStore {

    private static volatile PEERS INSTANCE = null;
    private final PeerStoreDatabase peerStoreDatabase;
    private final BootstrapDatabase bootstrapDatabase;

    private PEERS(PeerStoreDatabase peerStoreDatabase, BootstrapDatabase bootstrapDatabase) {
        this.peerStoreDatabase = peerStoreDatabase;
        this.bootstrapDatabase = bootstrapDatabase;
    }


    public static PEERS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PEERS.class) {
                if (INSTANCE == null) {
                    PeerStoreDatabase blocksDatabase = Room.databaseBuilder(context, PeerStoreDatabase.class,
                                    PeerStoreDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    BootstrapDatabase bootstrapDatabase = Room.databaseBuilder(context, BootstrapDatabase.class,
                                    BootstrapDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();


                    INSTANCE = new PEERS(blocksDatabase, bootstrapDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    @NonNull
    public List<Peer> getRandomPeers(int limit) {
        return bootstrapDatabase.bootstrapDao().getRandomPeers(limit);
    }

    @Override
    public void storeMultiaddr(@NonNull Multiaddr multiaddr) {
        peerStoreDatabase.peerStoreDao().insertMultiaddr(multiaddr);
    }

    @Override
    public void removeMultiaddr(@NonNull Multiaddr multiaddr) {
        peerStoreDatabase.peerStoreDao().removeMultiaddr(multiaddr);
    }

    @Override
    public void storePeer(@NonNull Peer peer) {
        bootstrapDatabase.bootstrapDao().insertPeer(peer);
    }

    @Override
    @Nullable
    public Multiaddr getMultiaddr(@NonNull PeerId peerId) {
        return peerStoreDatabase.peerStoreDao().getMultiaddr(peerId);
    }

    @Override
    public void removePeer(Peer peer) {
        bootstrapDatabase.bootstrapDao().removePeer(peer);
    }

}
