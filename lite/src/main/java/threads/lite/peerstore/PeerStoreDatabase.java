package threads.lite.peerstore;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;

@androidx.room.Database(entities = {Multiaddr.class}, version = 5, exportSchema = false)
@TypeConverters({PeerId.class, Multiaddr.class})
public abstract class PeerStoreDatabase extends RoomDatabase {

    public abstract PeerStoreDao peerStoreDao();

}
