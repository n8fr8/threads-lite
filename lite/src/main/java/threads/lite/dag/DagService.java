package threads.lite.dag;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.List;

import merkledag.pb.Merkledag;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Link;
import threads.lite.core.Session;
import unixfs.pb.Unixfs;

public class DagService {
    private final Session session;

    public DagService(Session session) {
        this.session = session;
    }

    @NonNull
    public static DagService createDagService(@NonNull Session session) {
        return new DagService(session);
    }

    @NonNull
    private Block getBlock(Cancellable cancellable, Cid cid) throws Exception {
        Block block = session.getBlockStore().getBlock(cid);
        if (block != null) {
            return block;
        }
        return session.getBlock(cancellable, cid);
    }

    @NonNull
    public Merkledag.PBNode getNode(@NonNull Cancellable cancellable,
                                    @NonNull Cid cid) throws Exception {
        Block block = getBlock(cancellable, cid);
        return block.getData();
    }


    @NonNull
    public static Merkledag.PBNode createRaw(byte[] bytes) {
        Unixfs.Data.Builder data = Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                .setFilesize(bytes.length)
                .setData(ByteString.copyFrom(bytes));

        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));

        return pbn.build();
    }


    public static DirectoryNode createDirectory(@NonNull List<Link> links) {
        Unixfs.Data.Builder builder = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory);
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        long fileSize = 0;
        for (Link link : links) {
            long size = link.getSize();
            fileSize = fileSize + size;
            builder.addBlocksizes(size);

            Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder()
                    .setName(link.getName())
                    .setTsize(link.getSize());

            lnb.setHash(ByteString.copyFrom(link.getCid().encoded()));

            pbn.addLinks(lnb.build());
        }
        builder.setFilesize(fileSize);
        byte[] unixData = builder.build().toByteArray();

        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), fileSize);

    }

    public static DirectoryNode createDirectory() {
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .build().toByteArray();
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        return new DirectoryNode(pbn.build(), 0);
    }


    public static boolean isDirectory(@NonNull Merkledag.PBNode node) throws Exception {
        Unixfs.Data unixData = DagReader.getData(node);

        return unixData.getType() == unixfs.pb.Unixfs.Data.DataType.Directory;
    }

    public static class DirectoryNode {
        private final Merkledag.PBNode node;
        private final long size;

        public DirectoryNode(Merkledag.PBNode node, long size) {
            this.node = node;
            this.size = size;
        }

        public Merkledag.PBNode getNode() {
            return node;
        }

        public long getSize() {
            return size;
        }
    }
}
