package threads.lite.asn1;

/**
 * Supported encoding formats.
 */
public interface ASN1Encoding {
    /**
     * DER - distinguished encoding rules.
     */
    String DER = "DER";

    /**
     * DL - definite length encoding.
     */
    String DL = "DL";

}
