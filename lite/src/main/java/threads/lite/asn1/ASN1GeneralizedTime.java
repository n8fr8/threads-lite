package threads.lite.asn1;

import java.io.IOException;

import threads.lite.asn1.util.Arrays;
import threads.lite.asn1.util.Strings;

/**
 * Base class representing the ASN.1 GeneralizedTime type.
 * <p>
 * The main difference between these and UTC time is a 4 digit year.
 * </p>
 * <p>
 * One second resolution date+time on UTC timezone (Z)
 * with 4 digit year (valid from 0001 to 9999).
 * </p><p>
 * Timestamp format is:  yyyymmddHHMMSS'Z'
 * </p><p>
 * <h2>X.690</h2>
 * This is what is called "restricted string",
 * and it uses ASCII characters to encode digits and supplemental data.
 *
 * <h3>11: Restrictions on BER employed by both CER and DER</h3>
 * <h4>11.7 GeneralizedTime </h4>
 * <p>
 * <b>11.7.1</b> The encoding shall terminate with a "Z",
 * as described in the ITU-T Rec. X.680 | ISO/IEC 8824-1 clause on
 * GeneralizedTime.
 * </p><p>
 * <b>11.7.2</b> The seconds element shall always be present.
 * </p>
 * <p>
 * <b>11.7.3</b> The fractional-seconds elements, if present,
 * shall omit all trailing zeros; if the elements correspond to 0,
 * they shall be wholly omitted, and the decimal point element also
 * shall be omitted.
 */
public class ASN1GeneralizedTime extends ASN1Primitive {
    final byte[] contents;

    /**
     * The correct format for this is YYYYMMDDHHMMSS[.f]Z, or without the Z
     * for local time, or Z+-HHMM on the end, for difference between local
     * time and UTC time. The fractional second amount f must consist of at
     * least one number with trailing zeroes removed.
     *
     * @param time the time string.
     * @throws IllegalArgumentException if String is an illegal format.
     */
    public ASN1GeneralizedTime(String time) {
        this.contents = Strings.toByteArray(time);
    }

    ASN1GeneralizedTime(byte[] bytes) {
        if (bytes.length < 4) {
            throw new IllegalArgumentException("GeneralizedTime string too short");
        }
        this.contents = bytes;

        if (!(isDigit(0) && isDigit(1) && isDigit(2) && isDigit(3))) {
            throw new IllegalArgumentException("illegal characters in GeneralizedTime string");
        }
    }

    static ASN1GeneralizedTime createPrimitive(byte[] contents) {
        return new ASN1GeneralizedTime(contents);
    }

    protected boolean hasFractionalSeconds() {
        for (int i = 0; i != contents.length; i++) {
            if (contents[i] == '.') {
                if (i == 14) {
                    return true;
                }
            }
        }
        return false;
    }

    protected boolean hasSeconds() {
        return isDigit(12) && isDigit(13);
    }

    protected boolean hasMinutes() {
        return isDigit(10) && isDigit(11);
    }

    private boolean isDigit(int pos) {
        return contents.length > pos && contents[pos] >= '0' && contents[pos] <= '9';
    }

    final boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.GENERALIZED_TIME, contents);
    }

    ASN1Primitive toDERObject() {
        return new DERGeneralizedTime(contents);
    }

    boolean asn1Equals(ASN1Primitive o) {
        if (!(o instanceof ASN1GeneralizedTime)) {
            return false;
        }

        return Arrays.areEqual(contents, ((ASN1GeneralizedTime) o).contents);
    }

    public int hashCode() {
        return Arrays.hashCode(contents);
    }
}
