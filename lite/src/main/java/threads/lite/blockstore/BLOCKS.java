package threads.lite.blockstore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;


public class BLOCKS implements BlockStore {

    private static volatile BLOCKS INSTANCE = null;
    private final BlocksStoreDatabase blocksStoreDatabase;

    private BLOCKS(BlocksStoreDatabase blocksStoreDatabase) {
        this.blocksStoreDatabase = blocksStoreDatabase;
    }

    @NonNull
    private static BLOCKS createBlocks(@NonNull BlocksStoreDatabase blocksStoreDatabase) {
        return new BLOCKS(blocksStoreDatabase);
    }

    public static BLOCKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BLOCKS.class) {
                if (INSTANCE == null) {
                    BlocksStoreDatabase blocksStoreDatabase = Room.databaseBuilder(context,
                                    BlocksStoreDatabase.class,
                                    BlocksStoreDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = BLOCKS.createBlocks(blocksStoreDatabase);
                }
            }
        }
        return INSTANCE;
    }


    @Override
    public void clear() {
        blocksStoreDatabase.clearAllTables();
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public Block getBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().getBlock(cid);
    }

    @Override
    public void storeBlock(@NonNull Block block) {
        blocksStoreDatabase.blockDao().insertBlock(block);
    }

    @Override
    public void deleteBlocks(@NonNull List<Cid> cids) {
        for (Cid cid : cids) {
            blocksStoreDatabase.blockDao().deleteBlock(cid);
        }
    }


}
