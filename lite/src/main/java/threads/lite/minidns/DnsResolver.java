package threads.lite.minidns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Supplier;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.minidns.record.A;
import threads.lite.minidns.record.AAAA;
import threads.lite.minidns.record.Data;
import threads.lite.minidns.record.Record;
import threads.lite.minidns.record.TXT;


public final class DnsResolver {
    public static final String DNS_ADDR = "dnsaddr=";
    public static final String DNS_LINK = "dnslink=";
    public static final Set<Inet4Address> STATIC_IPV4_DNS_SERVERS = new CopyOnWriteArraySet<>();
    public static final Set<Inet6Address> STATIC_IPV6_DNS_SERVERS = new CopyOnWriteArraySet<>();
    private static final String TAG = DnsResolver.class.getSimpleName();

    static {
        try {
            STATIC_IPV4_DNS_SERVERS.add(DnsUtility.ipv4From(IPFS.GOOGLE_DNS_SERVER_IP4));
        } catch (IllegalArgumentException e) {
            LogUtils.error(TAG, "Could not add static IPv4 DNS Server " + e.getMessage());
        }

        try {
            STATIC_IPV6_DNS_SERVERS.add(DnsUtility.ipv6From(IPFS.GOOGLE_DNS_SERVER_IP6));
        } catch (IllegalArgumentException e) {
            LogUtils.error(TAG, "Could not add static IPv6 DNS Server " + e.getMessage());
        }
    }

    @NonNull
    public static String resolveDnsLink(@NonNull DnsClient client, @NonNull String host) {

        Set<String> txtRecords = retrieveTxtRecords(client, "_dnslink.".concat(host));
        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_LINK)) {
                    return txtRecord.replaceFirst(DNS_LINK, "");
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, host + " " + throwable.getClass().getName());
            }
        }
        return "";
    }

    @NonNull
    private static InetAddress resolveDns6Address(
            @NonNull DnsClient client, @NonNull String host) throws UnknownHostException {
        try {

            DnsQueryResult result = client.query(host, Record.TYPE.AAAA);
            DnsMessage response = result.response;
            List<Record<? extends Data>> records = response.answerSection;
            for (Record<? extends Data> record : records) {
                Data payload = record.getPayload();
                if (payload instanceof AAAA) {
                    AAAA a = (AAAA) payload;
                    return a.getInetAddress();
                }
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return InetAddress.getByName(host);
    }

    @NonNull
    private static InetAddress resolveDns4Address(DnsClient client,
                                                  String host) throws UnknownHostException {
        try {
            DnsQueryResult result = client.query(host, Record.TYPE.A);
            DnsMessage response = result.response;
            List<Record<? extends Data>> records = response.answerSection;
            for (Record<? extends Data> record : records) {
                Data payload = record.getPayload();
                if (payload instanceof A) {
                    A a = (A) payload;
                    return a.getInetAddress();
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return InetAddress.getByName(host);
    }

    @NonNull
    private static Set<String> retrieveTxtRecords(@NonNull DnsClient client,
                                                  @NonNull String host) {
        Set<String> txtRecords = new HashSet<>();
        try {
            DnsQueryResult result = client.query(host, Record.TYPE.TXT);
            DnsMessage response = result.response;
            List<Record<? extends Data>> records = response.answerSection;
            for (Record<? extends Data> record : records) {
                Data payload = record.getPayload();
                if (payload instanceof TXT) {
                    TXT text = (TXT) payload;
                    txtRecords.add(text.getText());
                } else {
                    LogUtils.warning(TAG, payload.toString());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return txtRecords;
    }


    @Nullable
    private static Multiaddr resolveDns4Multiaddr(DnsClient dnsClient, Multiaddr multiaddress) {
        try {
            InetAddress address = resolveDns4Address(dnsClient, multiaddress.getHost());
            Objects.requireNonNull(address);
            return Multiaddr.upgrade(multiaddress, address);
        } catch (Throwable ignore) {
            LogUtils.error(TAG, multiaddress + " not supported");
        }
        return null;
    }

    @Nullable
    private static Multiaddr resolveDns6Multiaddr(DnsClient dnsClient, Multiaddr multiaddress) {
        try {
            InetAddress address = resolveDns6Address(dnsClient, multiaddress.getHost());
            Objects.requireNonNull(address);
            return Multiaddr.upgrade(multiaddress, address);
        } catch (Throwable ignore) {
            LogUtils.error(TAG, multiaddress + " not supported");
        }
        return null;
    }

    @Nullable
    public static Multiaddr resolveDns(@NonNull DnsClient dnsClient, @NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns6()) {
            return resolveDns6Multiaddr(dnsClient, multiaddr);
        }
        if (multiaddr.isDns4()) {
            return resolveDns4Multiaddr(dnsClient, multiaddr);
        }
        if (multiaddr.isDns()) {
            // https://github.com/libp2p/specs/tree/master/addressing
            // When the /dns protocol is used, the lookup may result in both IPv4 and IPv6
            // addresses, in which case IPv6 will be preferred. To explicitly resolve to IPv4 or
            // IPv6 addresses, use the /dns4 or /dns6 protocols, respectively.
            Multiaddr resolved = resolveDns6Multiaddr(dnsClient, multiaddr);
            if (resolved != null) {
                return resolved;
            }
            return resolveDns4Multiaddr(dnsClient, multiaddr);
        }
        throw new IllegalStateException("not a dns multiaddress");
    }

    @NonNull
    public static Set<Multiaddr> resolveDnsaddr(@NonNull DnsClient dnsClient,
                                                @NonNull IPV ipv,
                                                @NonNull Multiaddr multiaddr) {
        Set<Multiaddr> multiaddrs = new HashSet<>();
        if (!multiaddr.isDnsaddr()) {
            return multiaddrs;
        }
        String host = multiaddr.getHost();
        Objects.requireNonNull(host);

        try {
            PeerId peerId = multiaddr.getPeerId();
            Set<Multiaddr> addresses = resolveDnsaddrHost(dnsClient, ipv, host);
            for (Multiaddr addr : addresses) {
                PeerId cmpPeerId = addr.getPeerId();
                if (Objects.equals(cmpPeerId, peerId)) {
                    multiaddrs.add(addr);
                }
            }
        } catch (Throwable ignore) {
        }
        return multiaddrs;
    }


    @NonNull
    public static Set<Multiaddr> resolveDnsaddrHost(@NonNull DnsClient dnsClient,
                                                    @NonNull IPV ipv,
                                                    @NonNull String host) {
        return resolveDnsAddressInternal(dnsClient, ipv, host, new HashSet<>());
    }

    @NonNull
    public static Set<Multiaddr> resolveDnsAddressInternal(@NonNull DnsClient dnsClient,
                                                           @NonNull IPV ipv,
                                                           @NonNull String host,
                                                           @NonNull Set<String> hosts) {
        Set<Multiaddr> multiAddresses = new HashSet<>();
        // recursion protection
        if (hosts.contains(host)) {
            return multiAddresses;
        }
        hosts.add(host);

        Set<String> txtRecords = retrieveTxtRecords(dnsClient, "_dnsaddr." + host);

        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_ADDR)) {
                    String testRecordReduced = txtRecord.replaceFirst(DNS_ADDR, "");
                    Multiaddr multiaddr = Multiaddr.create(testRecordReduced);
                    if (multiaddr.isDnsaddr()) {
                        String childHost = multiaddr.getHost();
                        multiAddresses.addAll(resolveDnsAddressInternal(dnsClient, ipv,
                                childHost, hosts));
                    } else {

                        if (multiaddr.protocolSupported(ipv, true)) {
                            if (multiaddr.isDns() || multiaddr.isDns6() || multiaddr.isDns4()) {
                                Multiaddr resolved = resolveDns(dnsClient, multiaddr);
                                if (resolved != null) {
                                    multiAddresses.add(resolved);
                                }
                            } else {
                                multiAddresses.add(multiaddr);
                            }
                        }

                    }
                }
            } catch (Throwable throwable) {
                LogUtils.warning(TAG, "Not supported " + txtRecord);
            }
        }
        return multiAddresses;
    }

    @NonNull
    public static DnsClient getInstance(@NonNull Supplier<List<InetAddress>> settingSupplier) {
        return new DnsClient(settingSupplier, new DnsCache(128));
    }

}
