package threads.lite.cert;

public class RuntimeOperatorException
        extends RuntimeException {
    private final Throwable cause;

    public RuntimeOperatorException(String msg, Throwable cause) {
        super(msg);

        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }
}
