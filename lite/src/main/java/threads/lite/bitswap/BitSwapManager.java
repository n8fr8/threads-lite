package threads.lite.bitswap;


import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.BitSwap;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.PeerStore;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.DataHandler;


public class BitSwapManager implements BitSwap {

    private static final String TAG = BitSwapManager.class.getSimpleName();
    @NonNull
    private final Session session;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Set<Integer> failures = ConcurrentHashMap.newKeySet();
    @NonNull
    private final Set<Integer> handled = ConcurrentHashMap.newKeySet();
    @NonNull
    private final Set<Connection> peers = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BitSwapEngine bitSwapEngine;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final BitSwapRegistry bitSwapRegistry = new BitSwapRegistry();

    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public BitSwapManager(@NonNull Session session) {
        this.session = session;
        this.blockStore = session.getBlockStore();
        this.bitSwapEngine = new BitSwapEngine(blockStore);
        this.peerStore = session.getPeerStore();
    }

    @NonNull
    public static Integer convertMultiaddr(@NonNull Multiaddr multiaddr) {
        if (multiaddr.isCircuitAddress()) {
            return Objects.hash(multiaddr.getPeerId(), multiaddr.getRelayId());
        } else {
            return Arrays.hashCode(multiaddr.getPeerId().encoded());
        }
    }

    private CompletableFuture<Void> writeMessage(
            @NonNull Connection connection, @NonNull MessageOuterClass.Message msg) {

        CompletableFuture<Void> done = new CompletableFuture<>();
        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
                connection.close();
            }

            @Override
            public void streamTerminated(QuicStream quicStream) {
                if (!done.isDone()) {
                    done.completeExceptionally(new Throwable("stream terminated"));
                }
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.BITSWAP_PROTOCOL).contains(protocol)) {
                    throw new Exception("Protocol " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.BITSWAP_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(msg))
                            .thenApply(Stream::closeOutput)
                            .thenRun(() -> done.complete(null));
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) {
                LogUtils.error(TAG, "data writeMessage invoked");

            }
        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                connection.close();
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL));
            }
        });

        return done;

    }

    @Override
    public void close() {
        try {
            closed.set(true);
            bitSwapRegistry.close();
            peers.forEach(Connection::close);
            handled.clear();
            peers.clear();
            failures.clear();
            bitSwapEngine.close();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void connectProvider(ScheduledExecutorService executorService, Cancellable cancellable,
                                 Multiaddr multiaddr, boolean removeFromPeerStore) {

        Integer id = convertMultiaddr(multiaddr);

        if (failures.contains(id)) {
            // not possible to connect
            return;
        }

        if (handled.contains(id)) {
            return;
        }
        handled.add(id);

        if (cancellable.isCancelled()) {
            return;
        }


        int delay = 0; // fast lane when 0
        if (multiaddr.isCircuitAddress()) {
            delay = IPFS.BITSWAP_REQUEST_DELAY; // slow lane
        }

        try {
            executorService.schedule(() -> {
                try {
                    if (cancellable.isCancelled()) {
                        return;
                    }
                    LogUtils.info(TAG, "Try connection " + multiaddr);
                    Connection connection = session.dial(multiaddr, Parameters.getDefault());
                    LogUtils.info(TAG, "New connection " + multiaddr);
                    peers.add(connection);
                } catch (ConnectException e) {
                    LogUtils.info(TAG, "Failure ConnectException connection " + multiaddr);
                    failures.add(id);
                } catch (TimeoutException e) {
                    LogUtils.info(TAG, "Failure TimeoutException connection " + multiaddr);
                    failures.add(id);
                    if (removeFromPeerStore) {
                        peerStore.removeMultiaddr(multiaddr);
                    }
                } catch (InterruptedException ignore) {
                    // ignore
                }
            }, delay, TimeUnit.SECONDS);
        } catch (RejectedExecutionException ignore) {
            // standard failure
        }

    }


    private void findProviders(@NonNull Cancellable cancellable, @NonNull Cid cid) {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(
                Runtime.getRuntime().availableProcessors());

        try {
            session.findProviders(cancellable,
                    (multiaddr) ->
                            connectProvider(executorService, cancellable,
                                    multiaddr, false),
                    peerId -> {
                        Multiaddr multiaddr = peerStore.getMultiaddr(peerId);

                        if (multiaddr != null) {
                            LogUtils.info(TAG, "Found multiaddr of peerId " + multiaddr);
                            connectProvider(executorService, cancellable,
                                    multiaddr, true);
                        } else {
                            LogUtils.info(TAG, "No multiaddr of peerId " + peerId);
                        }
                    }, cid);
            executorService.shutdown();
        } finally {
            executorService.shutdownNow();
        }
    }


    @NonNull
    public Block runWantHaves(@NonNull Cancellable cancellable, @NonNull Cid cid) throws Exception {

        bitSwapRegistry.register(cid);

        AtomicBoolean providerStart = new AtomicBoolean(false);
        ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();

        try {
            Set<Integer> haves = new HashSet<>();


            while (!blockStore.hasBlock(cid)) {

                peers.addAll(session.getSwarm());

                for (Connection conn : peers) {
                    Integer id = convertMultiaddr(conn.remoteMultiaddr());
                    if (!conn.isConnected()) {
                        peers.remove(conn);
                        handled.remove(id);
                        continue;
                    }
                    if (!haves.contains(id)) {
                        haves.add(id);

                        LogUtils.info(TAG, "Schedule Haves " +
                                cid + " " + conn.getRemoteAddress());

                        writeMessage(conn, BitSwapMessage.create(
                                MessageOuterClass.Message.Wantlist.WantType.Have, cid))
                                .whenComplete((unused, throwable) -> {
                                    if (throwable != null) {
                                        LogUtils.error(TAG, throwable);
                                    }
                                });
                    }
                }

                if (cancellable.isCancelled()) {
                    throw new Exception("canceled operation");
                }

                if (session.isFindProvidersActive()) {

                    if (!providerStart.getAndSet(true)) {

                        int delay = 0;
                        if (!haves.isEmpty()) {
                            delay = IPFS.BITSWAP_REQUEST_DELAY * 2;
                        }

                        timer.schedule(() -> {

                            long start = System.currentTimeMillis();

                            try {
                                findProviders(cancellable, cid);
                            } catch (Throwable throwable) {
                                LogUtils.info(TAG, throwable.getClass().getSimpleName());
                            }

                            LogUtils.info(TAG, "Load Provider Finish "
                                    + cid +
                                    " onStart [" +
                                    (System.currentTimeMillis() - start) +
                                    "]...");

                            if (!cancellable.isCancelled()) {
                                providerStart.set(false);
                            }

                        }, delay, TimeUnit.SECONDS);

                    }

                }
            }
        } finally {
            timer.shutdown();
            timer.shutdownNow();
            bitSwapRegistry.unregister(cid);
        }

        return Objects.requireNonNull(blockStore.getBlock(cid));
    }


    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {

        if (isClosed()) throw new IllegalStateException("Bitswap is closed");

        try {
            Block block = blockStore.getBlock(cid);
            if (block == null) {
                AtomicBoolean done = new AtomicBoolean(false);
                LogUtils.info(TAG, "Block Get " + cid);

                try {
                    return runWantHaves(() -> cancellable.isCancelled() || done.get() || isClosed(),
                            cid);
                } finally {
                    done.set(true);
                }
            }
            return block;

        } finally {
            LogUtils.info(TAG, "Block Release  " + cid);
        }
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {

        BitSwapMessage msg = BitSwapMessage.create(bsm);

        for (Block block : msg.blocks()) {
            Cid cid = block.getCid();
            if (bitSwapRegistry.isRegistered(cid)) {

                LogUtils.info(TAG, "Received Block " + cid +
                        " " + connection.getRemoteAddress());

                blockStore.storeBlock(block);
                bitSwapRegistry.unregister(cid);

                // store for later reuse (maybe) [Never store a local address]
                if (!Multiaddr.isLocalAddress(connection.getRemoteAddress().getAddress())) {
                    peerStore.storeMultiaddr(connection.remoteMultiaddr());
                }
            }
        }

        for (Cid cid : msg.haves()) {
            if (bitSwapRegistry.isRegistered(cid)) {
                bitSwapRegistry.scheduleWants(connection, cid, new TimerTask() {
                    @Override
                    public void run() {
                        LogUtils.info(TAG, "Schedule Wants " +
                                cid + " " + connection.getRemoteAddress());
                        try {
                            writeMessage(connection, BitSwapMessage.create(
                                    MessageOuterClass.Message.Wantlist.WantType.Block, cid))
                                    .whenComplete((unused, throwable) -> {
                                        if (throwable != null) {
                                            LogUtils.error(TAG, throwable);
                                        }
                                    });
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                });
            }
        }

        bitSwapEngine.receiveMessage(connection, bsm);
    }

    public boolean isClosed() {
        return closed.get();
    }

}
