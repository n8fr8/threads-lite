# IPFS Lite Library

## General

The **IPFS Lite Library** implements a subset of the official IPFS library (https://ipfs.io/)
<br/>The subset functionality includes:
<ul>
<li>QUIC transport only, that means no TCP is supported</li>
<li>DHT is in client mode available</li>
<li>Hole Punching will be supported soon</li>
<li>Static relays will be supported soon, but they have to be provided by the user</li>
<li>Functionality for connecting to other peers are available</li>
<li>Autonat functionality is available in client mode</li>
</ul>
Supported Protocols (most in client mode):
<ul>
    <li>/mplex/6.7.0</li>
    <li>/libp2p/dcutr</li>
    <li>/libp2p/circuit/relay/0.2.0/hop</li>
    <li>/libp2p/circuit/relay/0.2.0/stop</li>
    <li>/noise</li>
    <li>/ipfs/kad/1.0.0</li>
    <li>/libp2p/autonat/1.0.0</li>
    <li>/multistream/1.0.0</li>
    <li>/ipfs/bitswap/1.2.0</li>
    <li>/ipfs/id/1.0.0</li>
    <li>/lite/push/1.0.0 (own protocol, used for shortcuts)</li>
    <li>/lite/pull/1.0.0 (own protocol, used for shortcuts)</li>
</ul>
<br/>

The **IPFS Lite Library** depends on the following libraries (apart from Android/JDK), which are
partly integrated due do necessary adoptions.
<ul>    
    <li>Kwik (https://github.com/ptrd/kwik) [integrated, QUIC transport]</li>
    <li>Minidns (https://github.com/MiniDNS/minidns) [integrated, library is used, because Android does not support TXT records correctly]</li>
    <li>Noise (https://github.com/rweather/noise-java) [integrated, NOISE security]</li>
</ul>

### Todos

This section contains the functionality, which should be still implemented:
<ul>
    <li>Static Relays: A list of static relays which should be provided by the user</li>
    <li>Punch Hole: Finalize and Test the functionality</li>
    <li>For the holepunch relays, this application should choose relay peers close to itself, instead of using the IPFS bootstrap nodes</li>
    <li>Make it production ready, means testing, check corner cases, memory and performance</li>
</ul>

### Limitations

This sections contains the limitations of this API. The limitations are in general by design,
because it is assumed that depending on the application the handling would be different.

<ul>
<li>Enhancement of transports: This limitation is by design. Only QUIC will be supported, due
to performance reasons.</li>
<li>Pinning Concept: A regular IPFS node has a pinning concept. This implementation has **no** pinning
at all. **All** data, within the data storage, are pinned automatically, unless the user deletes
the data. This limitation is by design, because each application requires a different
solution. There will be some applications, who simply clear the data regularly after a while,
while other applications may require a kind of "pinning", to keep track of data they want to keep or not.
Fore more details refer to the "Data Storage" and "Content Handling" sections within documentation.</li>
<li>Ipns Handling: The user has to track (store) ipns entries by himself. Especially the sequence 
number should be handled by the user. This limitation is by design, see section "IPNS Functions"
within documentation for more details.</li>
<li>Data Storage: Data within the data storage has no eol (EndOfLive), this differs from a 
regular IPFS data storage. That means no pinning possible, because everything is pinned. 
Also the relationship between data are not tracked anywhere. So the provided data storage just 
keeps blocks and not track the relation between them. The user is responsible for tracking the relationships. 
For example, when a user has downloaded two files, where both files are sharing some equal blocks (same Cid and data).
Deleting one of those files can lead to inconsistencies within the data storage, 
when the user has not take care, of not deleting the shared blocks. [Important]
Fore more details refer to the "Data Storage" and "Content Handling" sections within documentation.
</li>
<li>Files API: There is no high level API for interpreting the data within the data storage. Only
some basic APIs are available for evaluating Links, Directories, etc. This limitation is by design,
because it is assumed that lots of applications do not need this information anyway.</li>
<li>Relay v2 is only supported, static relays are also possible see (https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction)</li>
</ul>

## Documentation

### General

The main functionality of the **IPFS Lite Library** is located in the package **lite**. The
central API of the library is the IPFS class. This class reflects the entry point of all
functionality, which can be performed on this library. In order to get a singleton instance of this class,
you need an Context object from Android.

```
Context context = ....

IPFS ipfs = IPFS.getInstance(context);  // now it is possible to work with ipfs

```

### Connectivity
This section describes the connection functionality of this API.
<br/>
In order to connect to remote peers, you have to do some settings before.
<br/>
First you need a session. Simplified is a session an environment, what the connected peer can see of
your node and what he can do.
[Experts: especially with each session, comes a new bitswap and dht instantiation, 
which is handy for cleanups and performance]
<br/>
Second what you need is a so called "multiaddr". Not all kind of multi-addresses are supported. Only
those which can be used for the QUIC transport.
<br/>
Optionally you can defined parameters for the connection. This might include how much data should
be transformed, etc.
<br/>
After that you are ready to connect.
<br/>

```
Session session = ipfs.createSession(); // simple default session

Multiaddr multiaddr = ipfs.decodeMultiaddr("/ip4/149.178.68.145/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC8cUR"); // artificial address
                    
Parameters parameters = ipfs.getConnectionParameters(); // simple default parameters

// no timeout is set, it is set internally 
// for normal connections it is IPFS.CONNECT_TIMEOUT 5 sec
// for relayed connections it is IPFS.RELAY_CONNECT_TIMEOUT 30 sec (hole punch)
// others like dns are somewhere in between
Connection conn = ipfs.dial(session, multiaddr, parameters);
                    
... // ready to work with the connection                   

```

### Peer Info
This section describes briefly the identity protocol.
<br/>
After a successful connection to a peer. You can request the identity for example.

```
PeerInfo peerInfo = ipfs.getPeerInfo(conn).get(5, TimeUnit.SECONDS); // 5 seconds timeout

peerInfo.getAgent(); // peers agent info
peerInfo.getMultiaddrs(); // returns the peers multiaddrs
peerInfo.getProtocols(); // returns the peers protocols

```

### Store Content

This section describes briefly, how to store content in the data store and make it therefore
available for bitswap for exchange with remote peers.
<br/>
Note when storing data on the local data store, does not means that it is already visible for other
nodes. It is just visible for bitswap. Another step is necessary to finally make the data visible
to other nodes, which are described in the "Publish Content" section.
<br/>
The following functions are available for adding data. No need for an example, it should be
self-explanatory.

```
    @NonNull
    public Cid storeText(@NonNull Session session, @NonNull String text) throws Exception;
    
    @NonNull
    public Cid storeData(@NonNull Session session, byte[] data) throws Exception;
    
    @NonNull
    public Cid storeFile(@NonNull Session session, @NonNull File file) throws Exception;
    
    @NonNull
    public Cid storeInputStream(@NonNull Session session, @NonNull InputStream inputStream)
            throws Exception;
    
    @NonNull
    public Cid storeInputStream(@NonNull Session session, @NonNull InputStream inputStream,
                                @NonNull Progress progress, long size) throws Exception;
    
```

### Fetch Content

This section describes briefly, how to get the content of a known Cid object. There are plenty
functions available.
<br/>
The simple functions are prefixed with "get" (e.g. getText()), the more complicated ones are
prefixed with "fetch" (e.g. fetchToFile()). Some functions require an Cancellable object,
where others have a Progress indicator (which is also cancellable).
<br/>
Note: The content data is always stored in the "sessions data storage", which can be different
from the default storage.

```
try (Session session = ipfs.createSession()) {
    String test = "Moin";
    
    Cid cid = ipfs.storeText(session, test);
    assertNotNull(cid);
           
    String cmp = ipfs.getText(session, cid, () -> false);  // important the text is also stored in the sessions data storage     
    assertEquals(test, cmp);
}
```

### Publish Content

This section describes how to publish data to IPFS nodes, using the distributed hashtable
instantiated in a session.

```
try (Session session = ipfs.createSession()) {
    String test = "Moin";
    
    Cid cid = ipfs.storeText(session, test);
   
    Set<Multiaddr> providers = ConcurrentHashMap.newKeySet(); // sets where the providers are stored
                                                              // usefull when you just provide
                                                              // at least to 10 peers and then abort
    
    ipfs.provide(session, cid, providers::add, new TimeoutCancellable(() -> false, 30); 
    // Note: the function is configured not to be specific cancellable (just timeout)
    // Note: timeout will be after 30 sec   
    
}

```

### Find Content

This section describes how you find the providers who are claiming to have the data (Cid object) in
IPFS.

```
try (Session session = ipfs.createSession()) {

    Cid cid = ipfs.decodeCid("Qmaisz6NMhDB51cCvNWa1GMS7LU1pAxdF4Ld6Ft9kZEP2a"); // the data we are looking for

    Set<Multiaddr> providers = ConcurrentHashMap.newKeySet(); // all found providers are stored here

    
    ipfs.findProviders(session, providers::add, cid, new TimeoutCancellable(
                    () -> providers.size() > 10, 
                    30));                       // timeout in 30 sec, unless you have found more 
                                                // then 10 providers, which cancels the operation
    
}
```

### Find Peers

This function describes how to find peers within IPFS using the distributed hashtable (DHT).

```
try (Session session = ipfs.createSession()) {

    // the peer with the peerId you are looking for
    PeerId peerId = PeerId.fromBase58("12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
   
    AtomicBoolean found = new AtomicBoolean(false); // indicator for cancel the operation  
    
    ipfs.findPeer(session, peerId, multiaddr -> {
    
        // multiaddr found of the peer, now it is possible to connect
        
        found.set(true);           // set found to true, so the operation will be canceled
    }, new TimeoutCancellable(found::get, 30)); // timeout is set to 30 sec
    
    
}
```

### Find Closest Peers

This function describes how to find the closest peer relative to a given peerId within IPFS using
the distributed hashtable (DHT).

```
Set<Multiaddr> found = ConcurrentHashMap.newKeySet();

try (Session session = ipfs.createSession()) {

    // Note: 10 peers with their addresses should be found
    ipfs.findClosestPeers(session, ipfs.self(), found::add, 
          new TimeoutCancellable(() -> found.size() > 10, 30)); // timeout 30 sec

    assertTrue(found.size() > 10);

    for (Multiaddr multiaddr : found) {
        ... // do stuff with it
    }
}
```

### IPNS Functions

This section contains the two functions which are used for publishing an ipns - entry. An ipns entry
has the form like "ipns://k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0ddd"
You can now publish content (CID) with your identity.
**[Important]** The user is responsible, that the ipns content he likes to publish has an increasing
sequence number. Also the user is responsible to track the sequence number of other ipns addresses
he wants to follow.

```
Cid cid = ipfs.storeText("test"); // the content you want to publish

int sequence = 0; // an increasing sequence number for each publishing

Set<Multiaddr> providers = ConcurrentHashMap.newKeySet(); // this list will be filled with
                                                         // providers of your ipns entry

ipfs.publishName(session, sequene, cid, providers::add, new TimeoutCancellable(() -> false, 30));        
                       // ()-> false,  is a mechanism for canceling the task (here no cancel)
                       // timeout after 30 sec

// with the next command you can retrieve an ipns entry, even your own
// the function returns when an entry is found, which was signed by you and the sequence
// number is equal or greater the the given sequence number 
// [Note: this is an example to retrieve your own ipns entry -> "ipfs.self()"]
IpnsEntity entry = ipfs.resolveName(session, ipfs.self(), sequence, () -> false); 
                    
                    
entry.getSequence();               // returns 0
byte[] data = entry.getData();     // the data of the ipns object [see below]

Cid cmp = ipfs.decodeCid(new String(data).replaceFirst(IPFS.IPFS_PATH, ""));  // cid and cmp are equal

```

### Data Storage

This section describes the data storage within the API. The default data storage is a database which
is an instantiation of a persistent Android ROOM SQL database. The server is always working on the
main database directly, whether other operations depends on the defined data storage
(see interface DataStore) within a session (see interface Session).
<br/>
The default data storage is available with invoking the function ipfs.getBlockStore(),
<br/>
The following low level operations can be performed on the data storage directly.

```
    boolean hasBlock(@NonNull Cid cid);

    @Nullable
    Block getBlock(@NonNull Cid cid) throws Exception;

    @Nullable
    byte[] getData(@NonNull Cid cid);

    void deleteBlock(@NonNull Cid cid);

    void deleteBlocks(@NonNull List<Cid> cids);

    void putBlock(@NonNull Block block);

    void clear();
```

### Content Handling

This section describes the functionality for handling content. Note that in this API, only some low
level functionality are defined for handling and interpret the content data.
<br/>
No examples are given, the usage of the functions should be clear.

```

    // has the session block storage the cid block
    public boolean hasBlock(@NonNull Session session, @NonNull Cid cid);
       
   
    // returns all blocks of the cid from the session block store,
    // If the cid block contains links, also the links cid blocks are returned (recursive)
    @NonNull
    public List<Cid> getBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception;
    
    
    // remove the cid block (add all links blocks recursively) from the session block storage
    public void rmBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception;    
    
    
    @NonNull
    public Directory createEmptyDirectory(@NonNull Session session) throws Exception;
    
    // removes a link with the given name from the directory
    @NonNull
    public Directory removeFromDirectory(@NonNull Session session, @NonNull Cid dir, @NonNull String name) throws Exception;
    
    // add a link to to the directory  (link should be of Type File, but not tested)
    @NonNull
    public Directory addLinkToDirectory(@NonNull Session session, @NonNull Cid dir, @NonNull Link link) throws Exception;
    
    // creates a directory with the given links  (links should be of Type File, but not tested)
    @NonNull
    public Directory createDirectory(@NonNull Session session, @NonNull List<Link> links) throws Exception
    
     // function requires a cancellable, because the cid could be remote
    public boolean isDir(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable) throws Exception;
    
    // function requires a cancellable, because the cid could be remote
    public boolean hasLink(@NonNull Session session, @NonNull Cid cid,
                           @NonNull String name, @NonNull Cancellable cancellable) throws Exception;
    
    
    
    // function requires a cancellable, because the cid could be remote
    // this function return when all links have been evaluated
    // Note: only links are returned when they have a name (usually a file and directory)
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    @NonNull
    public List<Link> links(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                            @NonNull Cancellable cancellable) throws Exception;
            
                            
    // function requires a cancellable, because the cid could be remote
    // this function present immediately links, when evaluated
    // Note: only links are returned when they have a name (usually a file and directory)
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    public void links(@NonNull Session session, @NonNull Cid cid, @NonNull Consumer<Link> consumer,
                      boolean resolveChildren, @NonNull Cancellable cancellable)
            throws Exception;
    
    
    // function requires a cancellable, because the cid could be remote
    // return all links, also of type raw and unknown
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    @NonNull
    public List<Link> allLinks(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                               @NonNull Cancellable cancellable) throws Exception;
                               
                              
                                                       
```

### Server Functions

This section describes the server functions, which are available within this API.
<br/>
Currently only one server instantiation is possible. You can start the server with
the startServer function.
<br/>
The server is running in its own session, which is called server session. You can add your own
protocols to a server session. But this is more less the only thing you can do with the server
session (see class ServerSession)
<br/>
The following server functions are defined, where most of them are just for monitoring.

```

    // IFPS class
    @Nullable
    public Server getServer(){
        return host.getServer();
    }

    @NonNull
    public Server startServer(int port, 
                              @NonNull Consumer<Connection> connectConsumer, // notifier for incoming connections
                              @NonNull Consumer<Connection> closedConsumer, // notifier for closed connections
                              @NonNull Function<PeerId, Boolean> isGated) { This is a callback function, which allows you to block specific peerIds
        return host.startSever(port, connectConsumer, closedConsumer, reachabilityConsumer);
    }

  
    // Server class
    @NonNull
    public ServerSession getServerSession();
 
    public int getPort(); // returns the used port of the service
 
    public int numServerConnections(); // returns number of connections within the server
 
```

**Note** The following protocols are supported by the server
<br/>
"/multistream/1.0.0", "/ipfs/bitswap/1.2.0", "/ipfs/id/1.0.0" and "/lite/push/1.0.0", "/lite/pull/1.0.0" (own protocols)
<br/>
See class ServerSession for details.

### Autonat

The feature **autonat** will be invoked on the bootstrap nodes provided by IPFS. It is a single
function call, and when it succeeds it returns true, which indicates that you server can be dialed
from the outside. The returned public address will be visible in your
identity and used when publishing your content data.

```
AutonatResult result = ipfs.autonat(@NonNull Server server);

result.success();              // returns true when we have a dialable public address
result.dialableAddress();      // return the dialable address
result.getNatType();           // returns NatType (SYMMETRIC, FULL_CONE, RESTRICTED_CONE, 
                               // PORT_RESTRICTED_CONE, UNKNOWN), only matters when there is no 
                               // dialable address 
```

### Reservations

This section describes how to monitor and initiate a reservation on relays.
<br/> A reservation to a relay is required, so that your node might be accessible by others nodes 
via "hole punching" or in case of a static relays through a direct access.
<br/>
A static relay reservation is marked as Kind.STATIC and where a limited relay is marked as
Kind.LIMITED (see class Reservation).
<br>
Documentation of relays and how to configure them are documented under
(https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction)


```
    // return true, when it has reservations
    public boolean hasReservations() {
        return host.hasReservations();
    }

    // return the number of current reservations
    public int numReservations() {
        return reservations().size();
    }

    // this function returns all the valid reservations
    @NonNull
    public Set<Reservation> reservations() {
        return host.reservations();
    }

    // this function does the reservation [bound to the server]
    // Note: only reservation of version 2 are now supported
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    @NonNull 
    public Set<Reservation> reservations(@NonNull Server server, 
                                         @NonNull Set<Multiaddr> multiaddrs,
                                         long timeout) {
        return host.reservations(session, timeout);
    }
```

### Bootstrap

This section describes how to get access of the used bootstrap peers. The bootstrap peers are used
internally for the features autonat, reservations (hole punch relays [should change]) and also for
the first initial usage within the DHT (distributed hashtable).

```
Set<Multiaddr> bootstrap = ipfs.getBootstrap();
 
for (Multiaddr address : bootstrap) {
    PeerId peerId = address.getPeerId();
    ... 
}
```

### Security Functionality

This section contains functionality regarding your own node. Which includes access to your public
and private key. Your peer ID and peer information.

```
Keys keys = ipfs.getKeys(); // returns your public and private key (Ed25519)
keys.getPrivate();
keys.getPublic();

PeerInfo ownInfo = ipfs.getIdentity();
ownInfo.getMultiaddrs(); // Note: this contains the addresses you host are listening on

PeerId peerId = ipfs.self(); // returns your own peerId


```

### Session Functionality

The main aspects of a session are the handling of manually added connections and the instantiation
of bitswap. Bitswap is responsible for interchange data between peers.
<br/>
The user has the possibility to add connections to a session. This concept
is labeled here as "swarm". Those connections are automatically
used in bitswap for interchanging data, without knowing whether they actually have the specific
content. All connection which are manually created (dial-function) will automatically added
to the swarm.

```

    @NonNull
    Set<Connection> session.getSwarm();

```

<br/>
Another aspect of a session is the efficient handling of resources (like connections, cleanup, etc.)

```
// instead of having one single session for an aplication, sometimes it make sense
// to invoke specifc sessions for specific tasks (e.g. download scenario)
// The session here is created with its own block store
// the "BlockStoreMemory" object, which stores the incoming blocks in a memory block store
// the other parameters are :

boolean findProvidersActive = false; // when false, bitswap does not use the DHT to find for 
                                     // a content the peers who are offering it

Multiaddr multiaddr = ipfs.decodeMultiaddr(
            "/ip4/192.168.43.172/udp/4001/quic/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            

        
try (Session session = ipfs.createSession(new BlockStoreMemory(), findProvidersActive))) {

    Connection connection = ipfs.dial(sesssion, multiaddr, ipfs.getConnectionParameters());
    
    // now we can download some content we know that the peer is part of the session
    
    Cid cid = ipfs.decodeCid("Qmaisz6NMhDB51cCvNWa1GMS7LU1pAxdF4Ld6Ft9kZEP2a");
    byte[] data = ipfs.getData(session, cid, new TimeoutCancellable(10));
   
    // Note: the data is available in "BlockStoreMemory" (just as blocks)
    // but it is not available for the server (which based on the default internal block store)
    // and therefore can not be provided to other peers using bitswap  
   
    ...

}

// Note: a session is a AutoCloseable, when leaving this scope, all connections in 
// the swarm and in bitswap are closed automatically, moreover open tasks will be closed, and a
// cleanup occurs
// Note: the associated blockstore (database) is not cleared nor closed, it has to be done
// in a seperate step, if wished

```

<br/>
There are other aspects regarding a session, which are not described here. This includes security
aspects, for example what kind of protocols are supported for remote peer instantiated streams.
See the section "Protocol Functions" for details.

### Utils Functionality

This function described some useful functions within the IPFS API

```
// decodes a string into a Cid object
Cid cid = ipfs.decodeCid("Qmaisz6NMhDB51cCvNWa1GMS7LU1pAxdF4Ld6Ft9kZEP2a"); 

// decodes a string into a PeerId object
PeerId peerId = ipfs.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");  

// decodes a multiaddr from a string representation
Multiaddr multiaddr = ipfs.decodeMultiaddr("/ip4/149.178.68.145/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC8cUR");     
       
// resolves a dnsLink            
String link = ipfs.resolveDnsLink("blog.ipfs.io");


// resolves an dnsAddr [Note: for connection you can use directly the "dnsaddr" addresses]
Multiaddr dnsaddr = Multiaddr.create(
                "/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
List<Multiaddr> addresses = ipfs.resolveDnsaddr(dnsaddr);


// Utlity function, resolves a root Cid object till the path of links is reached
@NonNull
public Cid resolveCid(@NonNull Session session, @NonNull Cid root, @NonNull List<String> path,
                     @NonNull Cancellable cancellable) throws Exception;

```

### Protocol Functions

Each session has a list of protocols which are used for remote peer instantiated streams. Default
supported protocols are  "/multistream/1.0.0", "/ipfs/bitswap/1.2.0", "/ipfs/id/1.0.0"
and "/lite/push/1.0.0", "/lite/pull/1.0.0". Protocols are handled in the class Session, and the following functionality
is available.

```
    // returns all supported protocols within the session
    @NonNull
    Map<String, ProtocolHandler> getProtocols();

    // add a protocol handler to the supported list of protocols.
    // Note: a protocol handler is only invoked, when a remote peer initiate a
    // stream over an existing connection
    void addProtocolHandler(@NonNull ProtocolHandler protocolHandler);
```

The user has the possibility to enhance a session (and also the server session) with its own
protocol.

``` 
    IPFS ipfs = TestEnv.getTestInstance(context);

    // this is for testing, if protocol was successful
    CompletableFuture<String> done = new CompletableFuture<>();


    // (1) first step is: define a protocol handler
    ProtocolHandler testHandler = new ProtocolHandler() {
        @Override
        public String getProtocol() {
            return "/test";           // protocol starts with "/"
        }

        // is invoked, when the your protocol "/test" is requested
        @Override
        public void protocol(Stream stream) {
            // first thing to do is accept that you have the protocol
            // just replying the protocol name

            stream.writeOutput(DataHandler.encodeProtocols("/test"))
                    .thenApply(Stream::closeOutput);

            // closing the output is only valid for your protocol handler,
            // because you are not writing nothing more on the stream,
            // you just want to read-out data, which is done in
            // the data function
        }


        // is invoked, when data is arriving associated with your protocol
        @Override
        public void data(Stream stream, ByteBuffer data) {

            // now just reads out the data
            done.complete(new String(data.array()));
        }
    };


    // (2) when you add the handler to a server session, it will be used for incoming
    // connections too
    server.getServerSession().addProtocolHandler(testHandler);

    Map<String, ProtocolHandler> serverProtocols = server.getServerSession().getProtocols();
    assertNotNull(serverProtocols);

    assertTrue(serverProtocols.containsKey("/test"));
    assertTrue(serverProtocols.containsKey(IPFS.LITE_PUSH_PROTOCOL));
    assertTrue(serverProtocols.containsKey(IPFS.LITE_PULL_PROTOCOL));
    assertTrue(serverProtocols.containsKey(IPFS.MULTISTREAM_PROTOCOL));
    assertTrue(serverProtocols.containsKey(IPFS.IDENTITY_PROTOCOL));
    assertTrue(serverProtocols.containsKey(IPFS.BITSWAP_PROTOCOL));
    assertEquals(serverProtocols.size(), 6);

    try (Session session = ipfs.createSession()) {

        // add a the protocol with handler to a new session
        session.addProtocolHandler(testHandler);

        Map<String, ProtocolHandler> protocols = session.getProtocols();
        assertNotNull(protocols);

        assertTrue(protocols.containsKey("/test"));
        assertTrue(protocols.containsKey(IPFS.LITE_PUSH_PROTOCOL));
        assertTrue(protocols.containsKey(IPFS.LITE_PULL_PROTOCOL));
        assertTrue(protocols.containsKey(IPFS.MULTISTREAM_PROTOCOL));
        assertTrue(protocols.containsKey(IPFS.IDENTITY_PROTOCOL));
        assertTrue(protocols.containsKey(IPFS.BITSWAP_PROTOCOL));
        assertEquals(protocols.size(), 6);


        // now you have added the protocol to a session and server server session

        // (3) now invoke the test protocol via a connection and stream

        // for testing we are connecting to our own server
        Multiaddr ownLocalServerAddress = Multiaddr.getSiteLocalAddress(
                ipfs.self(), ipfs.getPort());
        Objects.requireNonNull(ownLocalServerAddress);

        Connection connection = ipfs.dial(session,
                ownLocalServerAddress, ipfs.getConnectionParameters());
        assertNotNull(connection);


        // create a stream and invoke the protocol as initiator

        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                fail();
            }

            @Override
            public void protocol(Stream stream, String protocol) {

                // the remote accepts your protocol, anyway check if it is your protocol
                // all protocols are going via this function (at least
                // the multistream are also visible here)
                if (Objects.equals(protocol, "/test")) {
                    // Note each data must be encoded (extra information like length of the
                    // data are added) IMPORTANT

                    // write a message "moin moin" and then close output
                    stream.writeOutput(DataHandler.encode("moin moin".getBytes()))
                            .thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                // nothing expected here
                throw new Exception("not expected here");
            }
        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                fail();
            } else {
                // Note: this invokes the protocol, the multistream must be used in front
                // for negotiate reasons
                stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL,
                        "/test"));
            }
        });

        // now wait for 5 sec timeout
        String message = done.get(5, TimeUnit.SECONDS);

        // when the server received the message, this should be true
        assertEquals(message, "moin moin");
    }
```